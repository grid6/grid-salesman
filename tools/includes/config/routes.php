<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
| example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
| $route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
| $route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
| $route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples: my-controller/index -> my_controller/index
|   my-controller/my-method -> my_controller/my_method
*/
$route['default_controller'] = 'AdminOAuth';
$route['404_override'] = 'Cust404';
$route['translate_uri_dashes'] = TRUE;
/*
| -------------------------------------------------------------------------
| TAPNOTE Routes
| -------------------------------------------------------------------------
*/
$route['d/(:any)/(:any)'] = 'Page/dynamic/$1/$2';
$route['login'] = 'AdminOAuth/index';
$route['auth/sales/token'] = 'AdminOAuth/token';
$route['auth/sales/logout'] = 'AdminOAuth/logout';
$route['menu'] = 'Claim/index';
$route['enroll'] = 'Claim/enroll';
$route['credit'] = 'Claim/credit';
$route['customers'] = 'Claim/cust';
$route['customers/save'] = 'Claim/saveCust';
$route['customers-save'] = 'Claim/saveCust';
$route['member/plans/(:any)'] = 'Claim/plans/$1';
$route['membership/done'] = 'Claim/plansdone';
$route['membership/error'] = 'Claim/error';
$route['checkout/(:any)/(:any)'] = 'Claim/checkidot/$1/$2';
$route['checkout/(:any)/(:any)/(:any)'] = 'Claim/checkidot/$1/$2/$3';
$route['checkout/(:any)/(:any)/(:any)/(:any)'] = 'Claim/checkidot/$1/$2/$3/$4';
$route['courses/checkout/(:any)/(:any)'] = 'Courses/checkidot/$1/$2';
$route['courses/checkout/(:any)/(:any)/(:any)'] = 'Courses/checkidot/$1/$2/$3';
$route['courses/checkout/(:any)/(:any)/(:any)/(:any)'] = 'Courses/checkidot/$1/$2/$3/$4';
$route['credit/checkout/(:any)/(:any)'] = 'Credit/checkidot/$1/$2';
$route['credit/checkout/(:any)/(:any)/(:any)'] = 'Credit/checkidot/$1/$2/$3';
$route['credit/checkout/(:any)/(:any)/(:any)/(:any)'] = 'Credit/checkidot/$1/$2/$3/$4';
$route['trainer/checkout/(:any)/(:any)'] = 'Trainer/checkidot/$1/$2';
$route['trainer/checkout/(:any)/(:any)/(:any)'] = 'Trainer/checkidot/$1/$2/$3';
$route['trainer/checkout/(:any)/(:any)/(:any)/(:any)'] = 'Trainer/checkidot/$1/$2/$3/$4';
$route['trainer/checkout/(:any)/(:any)/(:any)/(:any)/(:any)'] = 'Trainer/checkidot/$1/$2/$3/$4/$5';
$route['custcheck'] = 'Claim/check';
$route['api/register'] = 'Claim/scan';
$route['admin/claim/history'] = 'Claim/history';
