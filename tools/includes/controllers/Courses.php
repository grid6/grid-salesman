<?php
/**
 * Created by PhpStorm.
 * User: Win_10
 * Date: 28/02/2018
 * Time: 9:27
 */

class Courses extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->helper("form");
        $this->sess = $this->session->userdata("users");
        $sess = $this->sess;
        $this->user = isset($sess["user_id"]) ? $sess["user_id"] : 0;
        if(empty($this->user)){
            $this->session->sess_destroy();
            $this->session->set_flashdata('feedback', array('message'=>"your session has ended", 'error'=>true, 'status'=>'error'));
            redirect(site_url("login"));
        }
    }

    public function index(){
        $usrdata = $this->db->select("id, name")->get_where("sales", ["id"=>$this->user]);
        $data = [
          'users' => $usrdata->row()
        ];
        $this->load->view("courses", $data);
    }

    public function check($idcust=''){
        $date = $this->input->get("vzyt");
        $date = urldecode(base64_decode($date));
        $cuzt = null;
        if(empty($idcust) || empty($date)){
            redirect(base_url("trainer?msg=id is required"));
        }else{
          $sel = "id, CONCAT(first_name, ' ',last_name) as name, email, IFNULL(photo, 'https://placekitten.com/300/300') as photo";
          $custdata = $this->db->select($sel)->get_where("users", ["id"=>$idcust]);
          if(!$custdata || ($custdata && $custdata->num_rows() == 0)){
              redirect(base_url("trainer?msg=data customer not found"));
          }
          $bal=0;
          $sel2 = "id, balance, expired_at";
          $wall = $this->db->select($sel2)->get_where("wallets", ['holder_id'=>$idcust, 'holder_type'=>'App\Model\User']);
          if($wall && $wall->num_rows() > 0){
              $bal = (double)$wall->row()->balance;
              if(!empty($wall->row()->expired_at) && date("Y-m-d H:i:s", strtotime($wall->row()->expired_at)) <= date("Y-m-d H:i:s")){
                  $bal = ($bal <= 10) ? (double)$wall->row()->balance : 0;
              }
          }
          $cuzt = $custdata->row();
          $cuzt->balance = $bal;
        }
        $usrdata = $this->db->select("id, name")->get_where("sales", ["id"=>$this->user]);
        $this->db->join("classes", "classes_schedules.class_id = classes.id");
        $plandata = $this->db->select("classes_schedules.id as schedule_id, classes_schedules.time_start as time_start, classes_schedules.time_finish as time_finish, classes.*")->get_where("classes_schedules", ['schedule_date'=>$date]);
        // $plandata = $this->db->get_where("classes", "id IN(SELECT class_id FROM classes_schedules WHERE schedule_date = '$date')");

        $data = [
            'users'=>$usrdata->row(),
            'cust'=>$cuzt,
            'plan'=>$plandata,
            'date'=>$date
        ];
        $this->load->view("courselist", $data);
    }

    public function checkidot($idcust='', $idplan='', $sched=''){
        $date1 = $this->input->get("vzyt");
        $date = urldecode(base64_decode($date1));
        if(empty($idcust)){
            redirect(base_url("courses?msg=id is required"));
        }else{
          $sel = "id, CONCAT(first_name, ' ',last_name) as name, email, IFNULL(photo, 'https://placekitten.com/300/300') as photo, phone";
          $custdata = $this->db->select($sel)->get_where("users", ["id"=>$idcust]);
          if(!$custdata || ($custdata && $custdata->num_rows() == 0)){
            redirect(base_url("courses?msg=data customer not found"));
          }
        }

        if(empty($idplan)){
          redirect(base_url("courses?msg=class is required"));
        }else{
          $plandata = $this->db->get_where("classes", ["id"=>$idplan]);
          if(!$plandata || ($plandata && $plandata->num_rows() == 0)){
              redirect(base_url("courses?msg=data class not found"));
          }
        }

        if(empty($sched)){
            redirect(base_url("courses/check/$idcust?vzyt=$date1&msg=schedule is required"));
        }

        $sessdata = $this->db->get_where("classes_schedules", ["id"=>$sched]);
        if(!$sessdata || ($sessdata && $sessdata->num_rows() == 0)){
            redirect(base_url("courses/check/$idcust?vzyt=$date1&msg=schedule not found"));
        }


        $usrdata = $this->db->select("id, name")->get_where("sales", ["id"=>$this->user]);
        $url = "https://devapi.gridfitnesshub.com/";
        if(getenv("BASE_URL") == "https://salesxportal.gridfitnesshub.com"){
          $url = "https://xapi.gridfitnesshub.com/";
        }

        $currl = $url."public/$idcust/classes/$idplan/join/$sched";
        $rst = curl_api($currl, [], "GET");
        $headers=array();
        $data=explode("\n", $rst);
        array_shift($data);
        foreach($data as $part){
                $middle=explode(":",$part);
                error_reporting(0);
                $headers[trim($middle[0])] = trim($middle[1]);
        }
        $resval = (array)json_decode(end($data), true);

        $output = [
                'users'=>$usrdata->row(),
                'cust'=>$custdata->row(),
                'plan'=>$sessdata->row(),
                'trainer'=>$plandata->row(),
                'sessions'=>$sessions,
                'trans'=>$resval
        ];

        if($resval['error'] == 0){
            redirect(base_url("courses/done"));
        }else{
            var_dump($resval);
            exit();
        }
        // $this->load->view("coursesinvoice", $output);
    }

    public function done(){
        $this->load->view("coursesinvoice");
    }

    public function scan(){
        $code = $this->input->post("code");
        $err = "";
        $data = [];
        if(!empty($code)){
            $tick = $this->db->get_where("transticket", ['code'=>$code]);
            if($tick && $tick->num_rows() > 0){
                $ticc = $tick->row();
                if($ticc->status > 0){
                    $err = "this ticket has been claimed";
                }else{
                    $prod = $this->db->get_where("produk", ['id'=>$ticc->idproduk]);
                    if($prod && $prod->num_rows() > 0){
                        $data["produk"] = $prod->row()->nama;
                    }else $data["produk"] = "Tiket $code";
                    $uptik = [
                        "status"=>1,
                        'scanned_by'=>$this->user,
                        'scanned_at'=>date("Y-m-d H:i:s")
                    ];
                    $this->db->update("transticket", $uptik, ["id"=>$ticc->id]);
                    $ttlog = [
                        'ticket'=>$code,
                        'tgl'=>date("Y-m-d H:i:s"),
                        'status'=>1,
                        'created_by'=>$this->user
                    ];
                    $this->db->insert("ticketlog", $ttlog);
                }
            }else $err = "ticket not found";
        }else{
            $err = "ticket code is required";
        }
        $output = [
            "status"=>(!empty($data)) ? "success" : "error",
            "message"=>$err,
        ];
        if(!empty($data)) $output["data"] = $data;
        echo json_encode($output);
    }

    public function history(){
        $clam = $this->db->get_where("ticketlog", ["created_by"=>$this->user]);
        $claims = ($clam && $clam->num_rows() > 0) ? $clam->result() : [];
        $data_view = [
            'claim'=>$claims
        ];
        $this->load->view("claim", $data_view);
    }
}
