<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminOAuth extends CI_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        // Configure limits on our controller methods
        $this->sess = $this->session->userdata("users");
    }

    public function logout(){
        $sess = $this->sess;
        // var_dump($sess);
        // die;
        $user = isset($sess["user_id"]) ? $sess["user_id"] : 0;
        $this->session->unset_userdata('users');
        $this->session->set_flashdata('feedback', array('message'=>"your session has ended", 'error'=>true));
        redirect(base_url("login"));
    }

    public function index(){
        $sess = $this->sess;
        $user = isset($sess["user_id"]) ? $sess["user_id"] : 0;
        $roles = isset($sess["role"]) ? $sess["role"] : 0;
        if(!empty($user)){
            redirect(site_url("menu"));
        }
        $data["title"] = "Login";
        $this->load->view("login2", $data);
    }

    public function token(){
      try{
            $message = "";
            $stat = false;
            $sess = $this->sess;
            $user = isset($sess["user_id"]) ? $sess["user_id"] : 0;
            $data = array();
            if(!empty($user)){
                redirect(site_url("dashboard"));
            }else{
        				$user = $this->input->post("userdu");
                $pass = $this->input->post("passdu");
                $err = "";
                if(!empty($user) && !empty($pass)){
              					$sqs = $this->db->get_where("sales", array("email"=>$user));
              					if($sqs && $sqs->num_rows() > 0){
              						$usrow = $sqs->row();
              						$hash_pass = $usrow->password;
              						if(password_verify($pass, $hash_pass)){
              							 //set sessions
                             $arr_user = array("user_id"=>$usrow->id, "user_name"=>$usrow->nama);
                             $this->session->unset_userdata('users');
                             $this->session->set_userdata("users", $arr_user);
                             //set flashdata success
                             $this->session->set_flashdata('feedback', array('message'=>'login success', 'error'=>false, 'status'=>'success'));
                             //redirect to dashboard after login
               						   redirect(site_url('menu'));
              						}else{
                            $err = "pass doesnt match";
                            $this->session->set_flashdata('feedback', array('message'=>"password doesnt match", 'error'=>true));
                          }
              					}else{
                            $err = "user not found";
                            $this->session->set_flashdata('feedback', array('message'=>'username not found', 'error'=>true));
              					}
            		}else{
                        $err = "user and pass are required";
                        $this->session->set_flashdata('feedback', array('message'=>"username or password can't be empty", 'error'=>true));
            		}

                redirect(base_url()."?err=".$err);
            }

        }
        catch (Exception $catchMessage) {
            $this->output->set_status_header('404');
            $this->load->view('404');
		    }
    }
}
