<?php
/**
 * Created by PhpStorm.
 * User: Win_10
 * Date: 28/02/2018
 * Time: 9:27
 */

class Credit extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->helper("form");
        $this->sess = $this->session->userdata("users");
        $sess = $this->sess;
        $this->user = isset($sess["user_id"]) ? $sess["user_id"] : 0;
        if(empty($this->user)){
            $this->session->sess_destroy();
            $this->session->set_flashdata('feedback', array('message'=>"your session has ended", 'error'=>true, 'status'=>'error'));
            redirect(site_url("login"));
        }
    }

    public function index(){
        $usrdata = $this->db->select("id, name")->get_where("sales", ["id"=>$this->user]);

        $data = [
          'users'=>$usrdata->row()
        ];
        $this->load->view("buycredit", $data);
    }

    public function done(){
        $resval = $trainer = $plandata = [];
        if (isset($_GET['invoice']) && !empty($_GET['invoice'])) {
            $pays = $this->db->get_where("payment_transactions", ['invoice_no'=>urldecode($_GET['invoice'])]);
            if ($pays && $pays->num_rows() > 0) {
                $usrdata = $this->db->select("id, name")->get_where("sales", ["id"=>$this->user]);
                $resval = $pays->row_array();

                $custdata = $this->db->select("id, trim(concat(first_name, last_name)) as name, email, concat(phone_code, phone) as phone, gender, IFNULL(photo, '') as photo")->get_where("users", ["id"=>$resval["user_id"]]);
                $product = (!empty($resval["product"])) ? json_decode($resval["product"]) : [];
                $plandata = ($product) ? $product : [];
                $resval["payment_create"] = json_decode($resval["payment_create"], true);
                $output = [
                            'users'=>$usrdata->row(),
                            'cust'=>($custdata && $custdata->num_rows() > 0) ? $custdata->row() : null,
                            'plan'=>$plandata,
                            'trans'=>['data'=>$resval]
                ];
                $this->load->view("creditinvoice", $output);
            } else {
                redirect(base_url("menu")."?notfound=creditinvoice");
            }
        } else {
            redirect(base_url("Cust404"));
        }
    }

    public function package($idcust=''){
        if(empty($idcust)){
            redirect(base_url("credit?msg=id is required"));
        }else{
          $sel = "id, CONCAT(first_name, ' ',last_name) as name, email, IFNULL(photo, 'https://placekitten.com/300/300') as photo";
          $custdata = $this->db->select($sel)->get_where("users", ["id"=>$idcust]);
          if(!$custdata || ($custdata && $custdata->num_rows() == 0)){
            redirect(base_url("credit?msg=data customer not found"));
          }
        }
        $usrdata = $this->db->select("id, name")->get_where("sales", ["id"=>$this->user]);
        $plandata = $this->db->get("credit_packages");

        $data = [
            'users'=>$usrdata->row(),
            'cust'=>$custdata->row(),
            'plan'=>$plandata
        ];
        $this->load->view("packages", $data);
    }

    public function checkidot($idcust='', $idplan='', $channel='', $fee=0){
        if(empty($idcust)){
            redirect(base_url("credit?msg=id is required"));
        }else{
          $sel = "id, CONCAT(first_name, ' ',last_name) as name, email, IFNULL(photo, 'https://placekitten.com/300/300') as photo, phone";
          $custdata = $this->db->select($sel)->get_where("users", ["id"=>$idcust]);
          if(!$custdata || ($custdata && $custdata->num_rows() == 0)){
            redirect(base_url("credit?msg=data customer not found"));
          }
        }

        if(empty($idplan)){
          redirect(base_url("credit/package/$idcust?msg=id plan is required"));
        }else{
          $plandata = $this->db->get_where("credit_packages", ["id"=>$idplan]);
          if(!$plandata || ($plandata && $plandata->num_rows() == 0)){
              redirect(base_url("credit/package/$idcust?msg=data plan not found"));
          }
        }

        $usrdata = $this->db->select("id, name")->get_where("sales", ["id"=>$this->user]);
        $url = "https://devapi.gridfitnesshub.com/";
        if(getenv("BASE_URL") == "https://salesxportal.gridfitnesshub.com"){
          $url = "https://xapi.gridfitnesshub.com/";
        }

        if(empty($channel)){
            $rst = curl_api($url."api/payment/channels", [], "GET");
    				$headers=array();
    				$data=explode("\n",$rst);
    				array_shift($data);
    				foreach($data as $part){
    						$middle=explode(":",$part);
    						error_reporting(0);
    						$headers[trim($middle[0])] = trim($middle[1]);
    				}
    				$resval = (array)json_decode(end($data), true);
            // var_dump($resval);
            // exit();
            $output = [
                'users'=>$usrdata->row(),
                'cust'=>$custdata->row(),
                'plan'=>$plandata->row(),
                'channel'=>$resval
            ];
            $this->load->view("creditcheckout", $output);
        }else{
            $currl = $url."public/$idcust/credit/$idplan/buy/$channel/".$this->user."/$fee";
            $rst = curl_api($currl, [], "GET");
            $headers=array();
            $data=explode("\n", $rst);
            array_shift($data);
            foreach($data as $part){
                $middle=explode(":",$part);
                error_reporting(0);
                $headers[trim($middle[0])] = trim($middle[1]);
            }
            $resval = (array)json_decode(end($data), true);
            if (isset($resval['data']['invoice_no'])) {
                redirect(base_url("credit/done?invoice=".$resval['data']['invoice_no']));
            }
            // $resval['url'] = $currl;
            // var_dump($resval);
            // exit();
            $output = [
                'users'=>$usrdata->row(),
                'cust'=>$custdata->row(),
                'plan'=>$plandata->row(),
                'trans'=>$resval
            ];
            $this->load->view("creditinvoice", $output);
        }
    }

    public function check(){
        $code = $this->input->post("email");
        $err = "";
        $data = [];
        if(!empty($code)){
            $sel = "id, CONCAT(first_name, ' ',last_name) as name, email, IFNULL(photo, 'https://placekitten.com/300/300') as photo";
            $tick = $this->db->select($sel)->get_where("users", ['email'=>$code]);
            if($tick && $tick->num_rows() > 0){
                $ticc = $tick->row();
                $sel2 = "id, balance, expired_at";
                $wall = $this->db->select($sel2)->get_where("wallets", ['holder_id'=>$ticc->id, 'holder_type'=>'App\Model\User']);
                $bal=0;
                if($wall && $wall->num_rows() > 0){
                    $bal = (double)$wall->row()->balance;
                    if(!empty($wall->row()->expired_at) && date("Y-m-d H:i:s", strtotime($wall->row()->expired_at)) <= date("Y-m-d H:i:s")){
                        $bal = ($bal <= 10) ? (double)$wall->row()->balance : 0;
                    }
                }
                $ticc->balance = $bal;
                preg_match_all('/(?<=\b)\w/iu',$ticc->name,$matches);
                $ticc->avatar=mb_strtoupper(implode('',$matches[0]));
                $data = $ticc;
            }else $err = "user not found";
        }else{
            $err = "user email is required";
        }
        $output = [
            "status"=>(!empty($data)) ? "success" : "error",
            "message"=>$err,
        ];
        if(!empty($data)) $output["data"] = $data;
        echo json_encode($output);
    }

    public function scan(){
        $code = $this->input->post("code");
        $err = "";
        $data = [];
        if(!empty($code)){
            $tick = $this->db->get_where("transticket", ['code'=>$code]);
            if($tick && $tick->num_rows() > 0){
                $ticc = $tick->row();
                if($ticc->status > 0){
                    $err = "this ticket has been claimed";
                }else{
                    $prod = $this->db->get_where("produk", ['id'=>$ticc->idproduk]);
                    if($prod && $prod->num_rows() > 0){
                        $data["produk"] = $prod->row()->nama;
                    }else $data["produk"] = "Tiket $code";
                    $uptik = [
                        "status"=>1,
                        'scanned_by'=>$this->user,
                        'scanned_at'=>date("Y-m-d H:i:s")
                    ];
                    $this->db->update("transticket", $uptik, ["id"=>$ticc->id]);
                    $ttlog = [
                        'ticket'=>$code,
                        'tgl'=>date("Y-m-d H:i:s"),
                        'status'=>1,
                        'created_by'=>$this->user
                    ];
                    $this->db->insert("ticketlog", $ttlog);
                }
            }else $err = "ticket not found";
        }else{
            $err = "ticket code is required";
        }
        $output = [
            "status"=>(!empty($data)) ? "success" : "error",
            "message"=>$err,
        ];
        if(!empty($data)) $output["data"] = $data;
        echo json_encode($output);
    }

    public function history(){
        $clam = $this->db->get_where("ticketlog", ["created_by"=>$this->user]);
        $claims = ($clam && $clam->num_rows() > 0) ? $clam->result() : [];
        $data_view = [
            'claim'=>$claims
        ];
        $this->load->view("claim", $data_view);
    }
}
