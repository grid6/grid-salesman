<?php
/**
 * Created by PhpStorm.
 * User: Win_10
 * Date: 28/02/2018
 * Time: 9:27
 */

class Page extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper("form");
    }

    public function dynamic($type='', $slug=''){
        $slug = urldecode($slug);
        $type = urldecode($type);
        $page = $this->db->get_where("pages", ["type"=>$type, "slug"=>$slug]);
        $htmlpage = $title = "";
        if($page && $page->num_rows() > 0){
            $pg = $page->row();
            $title = $pg->name;
            $htmlpage = $pg->content;
        }else{
            header('Content-Type: application/json; charset=utf-8');
            echo json_encode(['code'=>404,'message'=>'page not found']);
            exit();
        }

        if(empty($title)){
            header('Content-Type: application/json; charset=utf-8');
            echo json_encode(['code'=>401,'message'=>'unauthozed']);
            exit();
        }

        $data = [
          'title' => $title,
          'html'  => $htmlpage
        ];
        $this->load->view("page", $data);
    }

    public function ticket(){
        $clam = $this->db->get_where("ticketlog", ["created_by"=>$this->user]);
        $claims = ($clam && $clam->num_rows() > 0) ? $clam->result() : [];
        $data_view = [
            'claim'=>$claims
        ];
        $this->load->view("claim", $data_view);
    }
}
