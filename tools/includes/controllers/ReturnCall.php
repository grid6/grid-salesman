<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ReturnCall extends CI_Controller
{

			function __construct()
			{
						parent::__construct();
		        // $this->auth();
			}

			/*Function to set JSON output*/
			public function output($Return=array()){
				/*Set response header*/
				header("Access-Control-Allow-Origin: *");
				header("Content-Type: application/json; charset=UTF-8");
				/*Final JSON response*/
				exit(json_encode($Return));
			}

			public function log()
			{
						$success = false;
						$resp_code = 0;
						$resp_message = "";
						$data_output = [];
						$data = (array)json_decode(file_get_contents('php://input'));
						if(!empty($data)){
								$data_insert = array();
								$order_id = $transaction_id = "";
								if(isset($data["transaction_id"])){
										$transaction_id = $data["transaction_id"];
										$data_insert["midtrans_id"] = $transaction_id;
								}
								if(isset($data["order_id"])){
										$order_id = $data["order_id"];
								}
								if(isset($data["payment_type"])) $data_insert["payment_type"]             = $data["payment_type"];
								if(isset($data["transaction_status"])){
										$transtat = $data["transaction_status"];
										if($transtat == 'captured' || $transtat == 'settlement'){
												$transtat = 'paid';
										}
										$data_insert["transaction_status"] = $transtat;
								}
								if(isset($data["fraud_status"])) $data_insert["fraud_status"]             = $data["fraud_status"];
								if(isset($data["approval_code"])) $data_insert["approval_code"]           = $data["approval_code"];
								if(isset($data["signature_key"])) $data_insert["signature_key"]           = $data["signature_key"];
								if(isset($data["transaction_time"])){
											$datetime = strtotime($data["transaction_time"]);
											$data_insert["transaction_time"] = date("Y-m-d H:i:s", $datetime);
								}
								// if(isset($data["status_message"])) $data_insert["status_message"]         = $data["status_message"];
								// if(isset($data["status_code"])) $data_insert["status_code"]               = $data["status_code"];
								if(isset($data["biller_code"])) $data_insert["biller_code"]               = $data["biller_code"];
								if(isset($data["bill_key"])) $data_insert["bill_key"]                     = $data["bill_key"];
								if(isset($data["custom_field3"]) || isset($data['custom_field2']) || isset($data['custom_field1'])){
										$custom_fields = [];
										if(isset($data['custom_field3'])) $custom_fields['custom_field3'] = $data["custom_field3"];
										if(isset($data['custom_field2'])) $custom_fields['custom_field2'] = $data["custom_field2"];
										if(isset($data['custom_field1'])) $custom_fields['custom_field1'] = $data["custom_field1"];
										if(!empty($custom_fields)){
												$data_insert['custom_fields'] = json_encode($custom_fields);
										}
								}
								if(isset($data["masked_card"])) $data_insert["masked_card"] = $data["masked_card"];
								if(!empty($data['bank'])){
											if(isset($data["bank"])) $data_insert["bank"] = $data["bank"];
								}
								// BNI && ATM Network
								if(!empty($data["va_numbers"])) {
											foreach($data['va_numbers'] as $van){
													$data_insert["bank"] = $van->bank;
													$data_insert["va_numbers"] = $van->va_number;
													$data_insert["payment_type"] = $van->bank."_va";
											}
								}
								// mandiri
								if ($data['payment_type'] == 'echannel') {
											$data_insert["bank"] = 'mandiri';
											$data_insert["va_numbers"] = $data['bill_key'];
								}
								// Permata
								if (!empty($data['permata_va_number'])) {
											$data_insert['bank'] = 'permata';
											$data_insert['va_numbers'] = $data['permata_va_number'];
								}

								// $cekshipment = $this->db->select("price")->order_by("tracking_id", "DESC")->get_where("shipment", array("order_id"=>$order_id));
								// $shipping_cost = ($cekshipment && $cekshipment->num_rows() > 0) ? (double)$cekshipment->row()->price : 0;

								if(isset($data["gross_amount"])){
											$data_insert["gross_amount"] = $data["gross_amount"];
								}

								if(isset($data["eci"])) $data_insert["eci"] = $data["eci"];
								if(isset($data["pdf_url"])) $data_insert["pdf_url"] = $data["pdf_url"];
								// $data_insert["log_json"]                                  = json_encode($data);
								// $data_insert["principle"]																	= "midtrans";
								$param_id = array('coupon_no'=>$data['order_id']);
								$customer_id = "0";
								$gestrans = $this->db->select("id, coupon_no, motorist_id")->get_where('coupons', array("coupon_no" => $order_id, "transaction_type"=>"MIDTRANS"));
								if($gestrans && $gestrans->num_rows() > 0){
										$customer_id = $gestrans->row()->motorist_id;
										$tranz_id = $gestrans->row()->id;
										$paramz_id["id"] = $tranz_id;
										$ins = $this->db->update("coupons", $data_insert, $paramz_id);
										if($ins){
												$resp_message = "insert success";
												$success = true;
												$data_output = $gestrans->row_array();
												if(isset($data['transaction_status'])) $data_output['status'] = $data['transaction_status'];
										}
										else{
												$resp_code = 400;
												$resp_message = "something went wrong when insert transaction";
										}
								}else{
										$resp_message = "data coupon not found";
										$resp_code = 404;
								}
						}else{
								$resp_message = "no data input";
								$resp_code = 400;
						}

						$responses = array(
								"success"=> $success,
								"response_code"=> $resp_code,
								"message" => $resp_message,
								"data" => $data_output,
						);

						$this->output($responses);
			}
}

/* End of file ReturnCall.php */
