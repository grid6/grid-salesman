<?php
/**
 * Created by PhpStorm.
 * User: Win_10
 * Date: 28/02/2018
 * Time: 9:27
 */

class Trainer extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->helper("form");
        $this->sess = $this->session->userdata("users");
        $sess = $this->sess;
        $this->user = isset($sess["user_id"]) ? $sess["user_id"] : 0;
        if(empty($this->user)){
            $this->session->sess_destroy();
            $this->session->set_flashdata('feedback', array('message'=>"your session has ended", 'error'=>true, 'status'=>'error'));
            redirect(site_url("login"));
        }
    }

    public function index(){
        $usrdata = $this->db->select("id, name")->get_where("sales", ["id"=>$this->user]);
        $tradata  = $this->db->select("id, name, level")->get_where("trainers", "level > 0");
        $data = [
          'users' => $usrdata->row(),
          'trainer' => ($tradata && $tradata->num_rows() > 0) ? $tradata->result() : []
        ];
        $this->load->view("trainer", $data);
    }

    public function done(){
        $resval = $trainer = $plandata = [];
        if (isset($_GET['invoice']) && !empty($_GET['invoice'])) {
            $pays = $this->db->get_where("payment_transactions", ['invoice_no'=>urldecode($_GET['invoice'])]);
            if ($pays && $pays->num_rows() > 0) {
                $usrdata = $this->db->select("id, name")->get_where("sales", ["id"=>$this->user]);
                $resval = $pays->row_array();

                $custdata = $this->db->select("id, trim(concat(first_name, last_name)) as name, email, concat(phone_code, phone) as phone, gender, IFNULL(photo, '') as photo")->get_where("users", ["id"=>$resval["user_id"]]);
                $product = (!empty($resval["product"])) ? json_decode($resval["product"]) : [];
                $plandata = ($product) ? $product : [];
                $sessions = ($product->sessions) ? $product->sessions : 0;
                $sessdata = $this->db->get_where("trainer_level_sessions", ['level_id'=>$product->level->id, 'sessions'=>$sessions]);
                $resval["payment_create"] = json_decode($resval["payment_create"], true);
                $output = [
                            'users'=>$usrdata->row(),
                            'cust'=>($custdata && $custdata->num_rows() > 0) ? $custdata->row() : null,
                            'plan'=> ($sessdata && $sessdata->num_rows() > 0) ? $sessdata->row() : null,
                            'trainer'=>$plandata,
                            'sessions'=>$sessions,
                            'trans'=>['data'=>$resval]
                ];
                $this->load->view("trainerinvoice", $output);
            } else {
                redirect(base_url("?notfound=invoice"));
            }
        } else {
            redirect(base_url("Cust404"));
        }
    }

    public function book(){
        $usrdata = $this->db->select("id, name")->get_where("sales", ["id"=>$this->user]);
        $tradata  = $this->db->select("id, name, level")->get_where("trainers", "level > 0");
        $data = [
          'users' => $usrdata->row(),
          'trainer' => ($tradata && $tradata->num_rows() > 0) ? $tradata->result() : []
        ];
        $this->load->view("bookpt", $data);
    }

    public function sessions($idcust='', $idtrainer=''){
        if(empty($idcust) || empty($idtrainer)){
            redirect(base_url("trainer?msg=id is required"));
        }else{
          $sel = "id, CONCAT(first_name, ' ',last_name) as name, email, IFNULL(photo, 'https://placekitten.com/300/300') as photo";
          $custdata = $this->db->select($sel)->get_where("users", ["id"=>$idcust]);
          if(!$custdata || ($custdata && $custdata->num_rows() == 0)){
              redirect(base_url("trainer?msg=data customer not found"));
          }
          $trainer = $this->db->get_where('trainers', ['id'=>$idtrainer]);
          if(!$trainer || ($trainer && $trainer->num_rows() == 0)){
              redirect(base_url("trainer?msg=data trainer not found"));
          }
        }
        $usrdata = $this->db->select("id, name")->get_where("sales", ["id"=>$this->user]);
        $plandata = $this->db->get_where("trainer_level_sessions", ['level_id'=>$trainer->row()->level]);

        $data = [
            'users'=>$usrdata->row(),
            'trainer'=>$trainer->row(),
            'cust'=>$custdata->row(),
            'plan'=>$plandata
        ];
        $this->load->view("sessions", $data);
    }

    public function checkidot($idcust='', $idplan='', $sessions=0, $channel='', $fee=0){
        if(empty($idcust)){
            redirect(base_url("credit?msg=id is required"));
        }else{
          $sel = "id, CONCAT(first_name, ' ',last_name) as name, email, IFNULL(photo, 'https://placekitten.com/300/300') as photo, phone";
          $custdata = $this->db->select($sel)->get_where("users", ["id"=>$idcust]);
          if(!$custdata || ($custdata && $custdata->num_rows() == 0)){
            redirect(base_url("credit?msg=data customer not found"));
          }
        }

        if(empty($idplan)){
          redirect(base_url("trainer?msg=trainer is required"));
        }else{
          $plandata = $this->db->get_where("trainers", ["id"=>$idplan]);
          if(!$plandata || ($plandata && $plandata->num_rows() == 0)){
              redirect(base_url("trainer?msg=data trainer not found"));
          }
        }

        if(empty($sessions)){
            redirect(base_url("trainer/sessions/$idcust/$idplan?msg=sessions is required"));
        }else{
            $sessdata = $this->db->get_where("trainer_level_sessions", ["sessions"=>$sessions, 'level_id'=>$plandata->row()->level]);
            if(!$sessdata || ($sessdata && $sessdata->num_rows() == 0)){
                redirect(base_url("trainer/sessions/$idcust/$idplan?msg=data plan not found"));
            }
        }

        $usrdata = $this->db->select("id, name")->get_where("sales", ["id"=>$this->user]);
        $url = "https://devapi.gridfitnesshub.com/";
        if(getenv("BASE_URL") == "https://salesxportal.gridfitnesshub.com"){
          $url = "https://xapi.gridfitnesshub.com/";
        }

        if(empty($channel)){
            $rst = curl_api($url."api/payment/channels", [], "GET");
    				$headers=array();
    				$data=explode("\n",$rst);
    				array_shift($data);
    				foreach($data as $part){
    						$middle=explode(":",$part);
    						error_reporting(0);
    						$headers[trim($middle[0])] = trim($middle[1]);
    				}
    				$resval = (array)json_decode(end($data), true);
            // var_dump($resval);
            // exit();
            $output = [
                'users'=>$usrdata->row(),
                'cust'=>$custdata->row(),
                'trainer'=>$plandata->row(),
                'plan'=>$sessdata->row(),
                'sessions'=>$sessions,
                'channel'=>$resval
            ];
            $this->load->view("trainercheckout", $output);
        }else{
            $currl = $url."public/$idcust/personal-trainer/$idplan/book/$sessions/$channel/".$this->user."/$fee";
            $rst = curl_api($currl, [], "GET");
            $headers=array();
            $data=explode("\n", $rst);
            array_shift($data);
            foreach($data as $part){
                $middle=explode(":",$part);
                error_reporting(0);
                $headers[trim($middle[0])] = trim($middle[1]);
            }
            $resval = (array)json_decode(end($data), true);
            if (isset($resval['data']['invoice_no'])) {
                redirect(base_url("trainers/done?invoice=".$resval['data']['invoice_no']));
            }
            // $resval['url'] = $currl;
            // var_dump($resval);
            // exit();
            $output = [
                'users'=>$usrdata->row(),
                'cust'=>$custdata->row(),
                'plan'=>$sessdata->row(),
                'trainer'=>$plandata->row(),
                'sessions'=>$sessions,
                'trans'=>$resval
            ];
            $this->load->view("trainerinvoice", $output);
        }
    }

    public function schedulecheck(){
        $url = "https://devapi.gridfitnesshub.com/";
        if(getenv("BASE_URL") == "https://salesxportal.gridfitnesshub.com"){
          $url = "https://xapi.gridfitnesshub.com/";
        }

        $rst = curl_api($url."public/trainer/schedule/join", $_POST, "POST");
        $headers=array();
        $data=explode("\n", $rst);
        array_shift($data);
        foreach($data as $part){
            $middle=explode(":",$part);
            error_reporting(0);
            $headers[trim($middle[0])] = trim($middle[1]);
        }
        $resval = (array)json_decode(end($data), true);
        echo json_encode($resval);
    }

    public function check(){
        $code = $this->input->post("email");
        $err = "";
        $data = [];
        if(!empty($code)){
            $sel = "id, CONCAT(first_name, ' ',last_name) as name, email, IFNULL(photo, 'https://placekitten.com/300/300') as photo";
            $tick = $this->db->select($sel)->get_where("users", ['email'=>$code]);
            if($tick && $tick->num_rows() > 0){
                $ticc = $tick->row();
                $sel2 = "id, balance, expired_at";
                $usid = $ticc->id;
                $trainers = $booked = [];
                $btrain = $this->db->query("SELECT trainer_booked.*, (SELECT COUNT(id) FROM trainer_joined WHERE user_id = trainer_booked.user_id AND trainer_id = trainer_booked.trainer) AS total_sessions FROM trainer_booked WHERE user_id = ? AND expired_at IS NOT NULL AND expired_at > NOW() HAVING sessions > total_sessions", [$usid]);
                if($btrain && $btrain->num_rows() > 0){
                    foreach($btrain->result() as $btr){
                        $sessions_left = $btr->sessions;
                        $trainers[$btr->trainer] = (isset($trainers[$btr->trainer])) ? $trainers[$btr->trainer] + $sessions_left : $sessions_left;
                    }
                }

                if(!empty($trainers)){
                    foreach($trainers as $tr => $vabt){
                        $trainz = $this->db->select("name")->get_where("trainers", ["id"=>$tr]);
                        $left = $this->db->select("COUNT(id) as total_sessions")->get_where("trainer_joined", ["user_id"=>$usid, "trainer_id"=>$tr]);
                        $total_left = ($left && $left->num_rows() > 0) ? $left->row()->total_sessions : 0;
                        $booked[] = [
                          'id'        => $tr,
                          'name'      => ($trainz && $trainz->num_rows() > 0) ? $trainz->row()->name : '',
                          'sessions'  => $vabt - $total_left
                        ];
                    }
                }

                $ticc->booked = $booked;

                $wall = $this->db->select($sel2)->get_where("wallets", ['holder_id'=>$ticc->id, 'holder_type'=>'App\Model\User']);
                $bal=0;
                if($wall && $wall->num_rows() > 0){
                    $bal = (double)$wall->row()->balance;
                    if(!empty($wall->row()->expired_at) && date("Y-m-d H:i:s", strtotime($wall->row()->expired_at)) <= date("Y-m-d H:i:s")){
                        $bal = ($bal <= 10) ? (double)$wall->row()->balance : 0;
                    }
                }
                $ticc->balance = $bal;
                preg_match_all('/(?<=\b)\w/iu',$ticc->name,$matches);
                $ticc->avatar=mb_strtoupper(implode('',$matches[0]));
                $data = $ticc;
            }else $err = "user not found";
        }else{
            $err = "user email is required";
        }
        $output = [
            "status"=>(!empty($data)) ? "success" : "error",
            "message"=>$err,
        ];
        if(!empty($data)) $output["data"] = $data;
        echo json_encode($output);
    }

    public function bookcheck($idcust='', $idplan='', $sched=''){
        $date1 = $this->input->get("vzyt");
        $date = urldecode(base64_decode($date1));
        if(empty($idcust)){
            redirect(base_url("courses?msg=id is required"));
        }else{
          $sel = "id, CONCAT(first_name, ' ',last_name) as name, email, IFNULL(photo, 'https://placekitten.com/300/300') as photo, phone";
          $custdata = $this->db->select($sel)->get_where("users", ["id"=>$idcust]);
          if(!$custdata || ($custdata && $custdata->num_rows() == 0)){
            redirect(base_url("courses?msg=data customer not found"));
          }
        }

        if(empty($idplan)){
          echo "trainer is required";
          // redirect(base_url("trainer/book?msg=trainer is required"));
        }else{
          $plandata = $this->db->get_where("trainers", ["id"=>$idplan]);
          if(!$plandata || ($plandata && $plandata->num_rows() == 0)){
              redirect(base_url("trainer/book?msg=data trainer not found"));
          }
        }

        $usrdata = $this->db->select("id, name")->get_where("sales", ["id"=>$this->user]);

        if(empty($sched)){
            $this->db->join("trainers", "trainers.id = trainer_schedules.trainer_id");
            $sessdata = $this->db->select("trainer_schedules.*, trainers.name as name")->get_where("trainer_schedules", ["trainer_id"=>$idplan, "schedule_date"=>$date]);
            $output = [
                'users'=>$usrdata->row(),
                'cust'=>$custdata->row(),
                'date'=>$date,
                'plan'=>$sessdata,
                'trainer'=>$plandata->row()
            ];
            $this->load->view("trainerschedules", $output);
        }else{
            $sessdata = $this->db->get_where("trainer_schedules", ["id"=>$sched]);
            if(!$sessdata || ($sessdata && $sessdata->num_rows() == 0)){
                redirect(base_url("trainer/bookcheck/$idcust?vzyt=$date1&msg=schedule not found"));
            }

            $url = "https://devapi.gridfitnesshub.com/";
            if(getenv("BASE_URL") == "https://salesxportal.gridfitnesshub.com"){
              $url = "https://xapi.gridfitnesshub.com/";
            }
            $data_post = [
              'user'=>$idcust,
              'trainer'=>$idplan,
              'schedule'=>$sched
            ];
            $rst = curl_api($url."public/trainer/schedule/join", $data_post, "POST");
            $headers=array();
            $data=explode("\n", $rst);
            array_shift($data);
            foreach($data as $part){
                $middle=explode(":",$part);
                error_reporting(0);
                $headers[trim($middle[0])] = trim($middle[1]);
            }
            $resval = (array)json_decode(end($data), true);

            $output = [
                    'users'=>$usrdata->row(),
                    'cust'=>$custdata->row(),
                    'plan'=>$sessdata->row(),
                    'trainer'=>$plandata->row(),
                    'sessions'=>$sessions,
                    'trans'=>$resval
            ];

            if($resval['error'] == 0){
                redirect(base_url("courses/done"));
            }else{
                var_dump($resval);
                exit();
            }
        }
        // $this->load->view("coursesinvoice", $output);
    }

    public function scan(){
        $code = $this->input->post("code");
        $err = "";
        $data = [];
        if(!empty($code)){
            $tick = $this->db->get_where("transticket", ['code'=>$code]);
            if($tick && $tick->num_rows() > 0){
                $ticc = $tick->row();
                if($ticc->status > 0){
                    $err = "this ticket has been claimed";
                }else{
                    $prod = $this->db->get_where("produk", ['id'=>$ticc->idproduk]);
                    if($prod && $prod->num_rows() > 0){
                        $data["produk"] = $prod->row()->nama;
                    }else $data["produk"] = "Tiket $code";
                    $uptik = [
                        "status"=>1,
                        'scanned_by'=>$this->user,
                        'scanned_at'=>date("Y-m-d H:i:s")
                    ];
                    $this->db->update("transticket", $uptik, ["id"=>$ticc->id]);
                    $ttlog = [
                        'ticket'=>$code,
                        'tgl'=>date("Y-m-d H:i:s"),
                        'status'=>1,
                        'created_by'=>$this->user
                    ];
                    $this->db->insert("ticketlog", $ttlog);
                }
            }else $err = "ticket not found";
        }else{
            $err = "ticket code is required";
        }
        $output = [
            "status"=>(!empty($data)) ? "success" : "error",
            "message"=>$err,
        ];
        if(!empty($data)) $output["data"] = $data;
        echo json_encode($output);
    }

    public function history(){
        $clam = $this->db->get_where("ticketlog", ["created_by"=>$this->user]);
        $claims = ($clam && $clam->num_rows() > 0) ? $clam->result() : [];
        $data_view = [
            'claim'=>$claims
        ];
        $this->load->view("claim", $data_view);
    }
}
