<?php
/**
 * Created by PhpStorm.
 * User: Win_10
 * Date: 28/02/2018
 * Time: 9:27
 */

class Scan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper("form");
    }

    public function qr($type="trainer"){
        $msg = $this->input->get("msg");
        $daz = $this->input->get("daz");
        $msg = urldecode($msg);
        $slug = $this->input->post("token");
        // $page = $this->db->get_where("users", ["email"=>$slug]);
        $name = "";
        $user = null;
        // if($page && $page->num_rows() > 0){
        //     $pg = $page->row();
        //     $user = $pg;
        //     $name = $pg->first_name." ".$pg->last_name;
        // }else{
        //     header('Content-Type: application/json; charset=utf-8');
        //     echo json_encode(['code'=>404,'message'=>'user not found for this email : '.$slug]);
        //     exit();
        // }

        $data = [
          'type'  => $type,
          'msg'   => $msg,
          'daz'   => $daz
        ];
        $this->load->view("qr", $data);
    }

    public function save($type="trainer"){
        $type = strtolower($type);
        $slugs = explode("-", $this->input->post("qr"));
        $usr = (isset($slugs[2])) ? $slugs[2] : "";
        $schedule = (isset($slugs[1])) ? $slugs[1] : "";
        if(empty($usr) || empty($schedule)){
            redirect(base_url("scan/qr/$type?msg=format qr value wrong&daz=error"));
        }
        $usrs = $this->db->get_where("users", ["id"=>$usr]);
        if(!$usrs || ($usrs && $usrs->num_rows() == 0)){
            redirect(base_url("scan/qr/$type?msg=user not found&daz=error"));
        }

        if($type == "trainer"){
            $trainer_id = $slugs[0];
            $trainer = $this->db->get_where("trainers", ["id"=>$trainer_id]);
            if(!$trainer || ($trainer && $trainer->num_rows() == 0)){
                redirect(base_url("scan/qr/$type?msg=data trainer not found&daz=error"));
            }
            $sched = $this->db->get_where("trainer_schedules", ["id"=>$schedule]);
            if(!$sched || ($sched && $sched->num_rows() == 0)){
                redirect(base_url("scan/qr/$type?msg=schedule not found&daz=error"));
            }else{
               if($sched->row()->trainer_id != $trainer_id){
                 redirect(base_url("scan/qr/$type?msg=the schedule is not match with the trainer schedule&daz=error"));
               }
            }
            $whr = [
              "user_id"=>$usr,
              "trainer_id"=>$trainer_id,
              "schedule_id"=>$schedule
            ];
            $joined = $this->db->get_where("trainer_joined", $whr);
            if($joined && $joined->num_rows() > 0){
              if(empty($joined->row()->attended_at)){
                  $this->db->update("trainer_joined", ["attended_at"=>date("Y-m-d H:i:s")], ["id"=>$joined->row()->id]);
                  $name = $usrs->row()->first_name." ".$usrs->row()->last_name;
                  $traname = $trainer->row()->name;
                  redirect(base_url("scan/qr/$type?msg=attendance $type success, welcome $name to class $traname&daz=success"));
              }else{
                  redirect(base_url("scan/qr/$type?msg=you already attend this schedule&daz=error"));
              }
            }else{
                redirect(base_url("scan/qr/$type?msg=data $type not found&daz=error"));
            }
        }else if($type == "class" || $type == "course"){
          $course_id = $slugs[0];
          $course = $this->db->get_where("classes", ["id"=>$course_id]);
          if(!$course || ($course && $course->num_rows() == 0)){
              redirect(base_url("scan/qr/$type?msg=data class not found&daz=error"));
          }
          $sched = $this->db->get_where("classes_schedules", ["id"=>$schedule]);
          if(!$sched || ($sched && $sched->num_rows() == 0)){
              redirect(base_url("scan/qr/$type?msg=schedule not found&daz=error"));
          }else{
             if($sched->row()->class_id != $course_id){
               redirect(base_url("scan/qr/$type?msg=the schedule is not match with the class schedule&daz=error"));
             }
          }

          $joined = $this->db->get_where("classes_joined", ["user_id"=>$usr, "class_id"=>$course_id, "schedule_id"=>$schedule]);
          if($joined && $joined->num_rows() > 0){
            if(empty($joined->row()->attended_at)){
                $this->db->update("classes_joined", ["attended_at"=>date("Y-m-d H:i:s")], ["id"=>$joined->row()->id]);
                $name = $usrs->row()->first_name." ".$usrs->row()->last_name;
                $traname = $course->row()->name;
                redirect(base_url("scan/qr/$type?msg=attendance $type success, welcome $name to class $traname&daz=success"));
            }else{
                redirect(base_url("scan/qr/$type?msg=you already attend this schedule&daz=error"));
            }
          }else{
              redirect(base_url("scan/qr/$type?msg=data $type not found&daz=error"));
          }
        }else{
            redirect(base_url("scan/qr/$type?msg=data $type not found&daz=error"));
        }
    }
}
