<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class M_Cart extends CI_Model{

			protected $table;
			protected $table_item;

			function __construct(){
					parent::__construct();
					$this->table = 'carts';
					$this->table_item = 'cart_items';
			}

			function insert($data){
					if(!isset($data["id"])) $data["id"] = get_uuid();
					$this->db->insert($this->table, $data);
					return $data["id"];
			}

			function update($data, $cond){
					if(!is_array($cond)){
							$cond = array('id'=>$cond);
					}
					return $this->db->update($this->table, $data, $cond);
			}

			function insert_item($data){
					if(!isset($data["id"])) $data["id"] = get_uuid();
					$this->db->insert($this->table_item, $data);
					return $data["id"];
			}

			function update_item($data, $cond){
					if(!is_array($cond)){
							$cond = array('id'=>$cond);
					}
					return $this->db->update($this->table_item, $data, $cond);
			}

			function delete_item($cond){
					if(!is_array($cond)){
							$cond = array('id'=>$cond);
					}
					return $this->db->delete($this->table_item, $cond);
			}

			function find($id) {
					$usdata = $this->db->get_where($this->table, array("id"=>$id));
					$rowdata = ($usdata && $usdata->num_rows() > 0) ? $usdata->row() : null;
					return $rowdata;
			}

			function findCond($where, $select='') {
					if(!empty($select)) $this->db->select($select);
					$usdata = $this->db->get_where($this->table, $where);
					return $usdata->row();
			}

			function findItem($where, $select='') {
					if(!empty($select)) $this->db->select($select);
					$usdata = $this->db->get_where($this->table_item, $where);
					return $usdata->row();
			}

			function lists($where, $select='') {
					if(!empty($select)) $this->db->select($select);
					$usdata = $this->db->get_where($this->table, $where);
					$sodata = ($usdata && $usdata->num_rows() > 0) ? $usdata->result() : [];
					return $sodata;
			}

			function list_items($where, $select='', $group_by='') {
					if(!empty($group_by)) $this->db->group_by($group_by);
					// if(!empty($order_by) && !empty($order_sort)) $this->db->order_by($order_by, $order_sort);
					if(!empty($select)) $this->db->select($select);
					$usdata = $this->db->get_where($this->table_item, $where);
					$sodata = ($usdata && $usdata->num_rows() > 0) ? $usdata->result() : [];
					return $sodata;
			}
}
