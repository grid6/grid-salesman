<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class M_Voucher extends CI_Model{

			protected $table;
			protected $table_detail;

			function __construct(){
					parent::__construct();
					$this->table = 'coupons';
					$this->table_detail = 'coupon_products';
					$this->load->model("M_Grosir", "grosir");
					$this->load->model("M_Promo", "promo");
			}

			function insert($data){
					if(!isset($data["id"])) $data["id"] = get_uuid();
					$this->db->insert($this->table, $data);
					return $data["id"];
			}

			function update($data, $cond){
					if(!is_array($cond)){
							$cond = array('id'=>$cond);
					}
					return $this->db->update($this->table, $data, $cond);
			}

			function insert_product($data){
					if(!isset($data["id"])) $data["id"] = get_uuid();
					$this->db->insert($this->table_detail, $data);
					return $data["id"];
			}

			function update_product($data, $cond){
					if(!is_array($cond)){
							$cond = array('id'=>$cond);
					}
					return $this->db->update($this->table_detail, $data, $cond);
			}

			function delete_product($cond){
					if(!is_array($cond)){
							$cond = array('id'=>$cond);
					}
					return $this->db->delete($this->table_detail, $cond);
			}

			function history($id, $select='', $status='all', $name='', $rpp=30, $page=1){
					$spage = ($page > 1) ? ($page - 1) * $rpp : 0;
					if(!empty($rpp) && $rpp != 'all') $this->db->limit($rpp, $spage);
					if($status != 'all') $this->db->where('status', $status);
					if(!empty($name)) $this->db->where('(coupon_no LIKE "%'.$name.'%" OR grosir_id IN(SELECT id FROM grosirs WHERE name LIKE "%'.$name.'%"))');
					if(!empty($select)) $this->db->select($select);
					$sdata = $this->db->order_by("created_at", "DESC")->get_where($this->table, array("motorist_id"=>$id, "status !="=>9));
					$data = [];
					if($sdata && $sdata->num_rows() > 0){
							$data = $sdata->result();
					}
					return $data;
			}
			function total_history($id, $status='all', $name=''){
					if($status != 'all') $this->db->where('status', $status);
					if(!empty($name)) $this->db->where('(coupon_no LIKE "%'.$name.'%" OR grosir_id IN(SELECT id FROM grosirs WHERE name LIKE "%'.$name.'%"))');
					$sdata = $this->db->select("COUNT(id) as total")->get_where($this->table, array("motorist_id"=>$id, "status !="=>9));
					$tot = 0;
					if($sdata && $sdata->num_rows() > 0){
							$tot = (int)$sdata->row()->total;
					}
					return $tot;
			}

			function lists($cond=null, $select='', $rpp=20, $page=1){
					$spage = ($page > 1) ? ($page - 1) * $rpp : 0;
					if(!empty($rpp) && $rpp != 'all') $this->db->limit($rpp, $spage);
					if(!empty($cond)) $this->db->where($cond);
					if(!empty($select)) $this->db->select($select);
					$sdata = $this->db->order_by("created_at", "DESC")->get($this->table);
					$data = [];
					if($sdata && $sdata->num_rows() > 0){
							$data = $sdata->result();
					}
					return $data;
			}

			function find($id) {
					$usdata = $this->db->get_where($this->table, array("id"=>$id));
					return $usdata->row();
			}

			function total_success($uid) {
					$usdata = $this->db->select("COUNT(id) as total")->get_where($this->table, array("motorist_id"=>$uid, "status"=>5));
					$rowtotal = ($usdata && $usdata->num_rows() > 0) ? (int)$usdata->row()->total : 0;
					return $rowtotal;
			}

			function findCond($where, $select='') {
					if(!empty($select)) $this->db->select($select);
					$usdata = $this->db->get_where($this->table, $where);
					return $usdata->row();
			}

			function findProduct($where, $select='') {
					if(!empty($select)) $this->db->select($select);
					$usdata = $this->db->get_where($this->table_detail, $where);
					return $usdata->row();
			}

			function listProduct($where, $select='', $group_by='') {
					if(!empty($group_by)) $this->db->group_by($group_by);
					if(!empty($select)) $this->db->select($select);
					$usdata = $this->db->get_where($this->table_detail, $where);
					$row = ($usdata && $usdata->num_rows() > 0) ? $usdata->result() : [];
					return $row;
			}

			function latest_code(){
					$rando = 0;
					$date = date("dmy");
					$codebribe = "CP-".$date."-";
					$this->db->order_by("coupon_no", "DESC");
					$psql = $this->db->limit(1, 0)->order_by("coupon_no", "DESC")->select("coupon_no")->like("coupon_no", $codebribe)->get($this->table);
					if($psql && $psql->num_rows() > 0){
							$vcode = $psql->row()->coupon_no;
							$randos = explode("-", $vcode);
							$rando = end($randos);
					}
					$rando = (int)$rando + 1;
					$vendor_code = $codebribe.sprintf("%04d", $rando);
					return $vendor_code;
			}

			function detail($id){
					$data_show = null;
					$cdata = $this->db->select("id, coupon_no, grosir_id as id_grosir, promo_id as id_promo, reasons, total_product, total_save, total_price, status, created_at, IFNULL(approved_at, '') as approved_at, IFNULL(rejected_at, '') as rejected_at, IFNULL(redeemed_at, '') as redeemed_at, IFNULL(succeeded_at, '') as succeeded_at")->get_where($this->table, array("id"=>$id));
					if($cdata && $cdata->num_rows() > 0){
							$data_show = $cdata->row();
							if(isset($data_show->total_price) && isset($data_show->total_save)){
									$subtotal = $data_show->total_price + $data_show->total_save;
									$data_show->subtotal = (string)$subtotal;
							}
							/* show status */
							switch($data_show->status){
												case 0 : $status = "expired";break;
												case 1 : $status = "requested";break;
												case 2 : $status = "approved";break;
												case 3 : $status = "rejected";break;
												case 4 : $status = "redeemed";break;
												case 5 : $status = "success";break;
												default : $status = "requested";break;
							}
							$data_show->status = $status;
							$data_show->reasons = (string)$data_show->reasons;
							/* checking data created_at*/
							if(isset($data_show->created_at)){
									$data_show->created_at = date_id($data_show->created_at, 'j M Y H:i');
							}
							/* checking data approved_at*/
							if(isset($data_show->approved_at) && !empty($data_show->approved_at)){
									$data_show->approved_at = date_id($data_show->approved_at, 'j M Y H:i');
							}
							/* checking data rejected_at*/
							if(isset($data_show->rejected_at) && !empty($data_show->rejected_at)){
									$data_show->rejected_at = date_id($data_show->rejected_at, 'j M Y H:i');
							}
							/* checking data redeemed_at*/
							if(isset($data_show->redeemed_at) && !empty($data_show->redeemed_at)){
									$data_show->redeemed_at = date_id($data_show->redeemed_at, 'j M Y H:i');
							}
							/* checking data succeeded_at*/
							if(isset($data_show->succeeded_at) && !empty($data_show->succeeded_at)){
									$data_show->succeeded_at = date_id($data_show->succeeded_at, 'j M Y H:i');
							}
							/* checking data grosir*/
							if(isset($data_show->id_grosir)){
									$grosirs = $this->grosir->find($data_show->id_grosir, 'id, name, IFNULL(latitude, "") as latitude, IFNULL(longitude, "") as longitude, IFNULL(address, "") as address, province_code as province_id, district_code as district_id, city_code as city_id, subdistrict_code as subdistrict_id, area_service_id as area_service');
									$data_show->grosir = (!empty($grosirs)) ? singleGrosirOnly($grosirs) : null;
									unset($data_show->id_grosir);
							}
							/* checking promo product*/
							if(isset($data_show->id_promo)){
										$prosql = $this->promo->find(["id"=>$data_show->id_promo], "id, code, DATE_ADD(start_date, INTERVAL 7 HOUR) as start_date, DATE_ADD(end_date, INTERVAL 7 HOUR) as end_date");
										if(isset($prosql->id)){
												$data_show->promo = $prosql;
												unset($data_show->id_promo);
										}
							}

							$wherez = ["coupon_id"=>$id];
							$ddata = $this->db->select("id, promo_product_id, product_id, before_price, price, quantity, subtotal, comp_percent, total_save")->get_where($this->table_detail, $wherez);
							$data = ($ddata && $ddata->num_rows() > 0) ? $ddata->result() : [];
							$data_show->order_summary = resourceCouponProduct($data);
							$arr_potongan = $total_before = [];
							if(count($data_show->order_summary) > 0){
										foreach($data_show->order_summary as $oord){
												$arr_potongan[] = $oord->total_save;
												$total_before[] = $oord->total_before;
										}
										if(!empty($arr_potongan)) $data_show->total_save = (string)array_sum($arr_potongan);
										if(!empty($total_before)) $data_show->subtotal = (string)array_sum($total_before);
							}
					}
					return $data_show;
			}
}
