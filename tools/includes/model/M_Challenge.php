<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class M_Challenge extends CI_Model{

			protected $table;
			protected $table_detail;
			protected $table_motor;
			protected $table_warung;
			protected $table_sales;
			protected $table_coupon;
			protected $table_coupon_product;

			function __construct(){
					parent::__construct();
					$this->table = 'challenge';
					$this->table_detail = 'challenger';
					$this->table_motor = 'users_motorist';
					$this->table_warung = 'warungs';
					$this->table_sales = 'warung_sales';
					$this->table_coupon = 'coupons';
					$this->table_coupon_product = 'coupon_products';
			}

			function getData($cond=null, $rpp=20, $page=1){
					$spage = ($page > 1) ? ($page - 1) * $rpp : 0;
					if(!empty($rpp) && $rpp != 'all') $this->db->limit($rpp, $spage);
					if(!empty($cond)) $this->db->where($cond);
					$rsdata = $this->db->select("id, subject, IFNULL(subtitle, '') as subtitle, activity, activity_point, activity_target, IFNULL(image, '') as images, start_at, expired_at")->get($this->table);
					$data_ret = ($rsdata && $rsdata->num_rows() > 0) ? $rsdata->result() : [];
					return $data_ret;
			}

			function insert($data){
					if(!isset($data["id"])) $data["id"] = get_uuid();
					$this->db->insert($this->table, $data);
					return $data["id"];
			}

			function join($data){
					if(!isset($data["id"])) $data["id"] = get_uuid();
					$this->db->insert($this->table_detail, $data);
					return $data["id"];
			}

			function update($data, $cond){
					if(!is_array($cond)){
							$cond = array('id'=>$cond);
					}
					return $this->db->update($this->table, $data, $cond);
			}

			function update_detail($data, $cond){
					if(!is_array($cond)){
							$cond = array('id'=>$cond);
					}
					return $this->db->update($this->table_detail, $data, $cond);
			}

			function delete($cond){
					if(!is_array($cond)){
							$cond = array('id'=>$cond);
					}
					return $this->db->delete($this->table, $cond);
			}

			function disjoin($cond){
					if(!is_array($cond)){
							$cond = array('id'=>$cond);
					}
					return $this->db->delete($this->table_detail, $cond);
			}

			function detail($id, $uid=''){
					$data_show = null;
					$cdata = $this->db->select("id, subject, IFNULL(subtitle, '') as subtitle, link, IFNULL(image, '') as images, activity, IFNULL(content, '') as content, activity_target, activity_point, expired_at, created_at")->get_where($this->table, array("id"=>$id));
					if($cdata && $cdata->num_rows() > 0){
							$data_show = $cdata->row();
							if(isset($data_show->expired_at)) $data_show->expired_at = date_id($data_show->expired_at, 'j M Y');
							if(isset($data_show->activity_point)) $data_show->activity_point = $data_show->activity_point;
							$wherez = ["challenge_id"=>$id];
							if(!empty($uid)) $wherez["motorist_id"] = $uid;
							$ddata = $this->db->select("CONCAT(users_motorist.first_name, ' ', users_motorist.last_name) as motorist, target_achieved, challenger.created_at as join_at")->join($this->table_motor, $this->table_detail.".motorist_id = ".$this->table_motor.".id")->get_where($this->table_detail, $wherez);
							$tdata = $this->db->select("COUNT(id) as total")->get_where($this->table_detail, ["challenge_id"=>$id]);
							$total = ($tdata && $tdata->num_rows() > 0) ? $tdata->row()->total : "0";
							$data = ($ddata && $ddata->num_rows() > 0) ? $ddata->row() : null;
							$data_show->total_join = $total;
							$data_show->is_join = (!empty($data)) ? true : false;
							if(isset($data->motorist)) $data->motorist = trim($data->motorist);
							$target_achieved=0;
							if(isset($data->target_achieved) && isset($data_show->activity_target)){
									$data->target_achieved = $data->target_achieved;
									$act_target = (!empty($data_show->activity_target)) ? $data_show->activity_target : 1;
									$target_achieved = ($data->target_achieved / $data_show->activity_target) * 100;
							}
							$data_show->percent_target = (string)$target_achieved;
							$data_show->join_data = $data;
					}
					return $data_show;
			}

			function find($id) {
					$usdata = $this->db->get_where($this->table, array("id"=>$id));
					$rowdata = ($usdata && $usdata->num_rows() > 0) ? $usdata->row() : null;
					return $rowdata;
			}

			function data_join($id, $uid){
					$wherez = ["challenge_id"=>$id, 'motorist_id'=>$uid];
					$ddata = $this->db->select("CONCAT(users_motorist.first_name, ' ', users_motorist.last_name) as motorist, target_achieved, challenger.created_at as join_at")->join($this->table_motor, $this->table_detail.".motorist_id = ".$this->table_motor.".id")->get_where($this->table_detail, $wherez);
					$data = ($ddata && $ddata->num_rows() > 0) ? $ddata->row() : null;
					return $data;
			}

			function find_detail($cond) {
					$usdata = $this->db->get_where($this->table_detail, $cond);
					$rowdata = ($usdata && $usdata->num_rows() > 0) ? $usdata->row() : null;
					return $rowdata;
			}

			function complete_list($activity='', $motorist_id=''){
					$gdata = $this->db->select("id, challenge_id, target_achieved, challenge_log, created_at")->get_where($this->table_detail, "motorist_id = '$motorist_id' AND challenge_id IN(SELECT id FROM challenge WHERE activity = '$activity')");
					$lquery = $this->db->last_query();
					if($gdata && $gdata->num_rows() > 0){
							foreach($gdata->result() as $gdat){
									$enrolldate = date("Y-m-d H:i:s", strtotime("+7 hours", strtotime($gdat->created_at)));
									$mdat = $this->db->select("id, activity, activity_target, activity_point, data, start_at, expired_at")->get_where($this->table, ["id"=>$gdat->challenge_id]);
									$lqq = $this->db->last_query();
									if($mdat && $mdat->num_rows() > 0){
											$mrow = $mdat->row();
											if($mrow->start_at <= date("Y-m-d") && $mrow->expired_at >= date("Y-m-d")){
													switch($activity){
															case 'complete-profile' :
															case 'profile' : 	$data_json = json_decode($mrow->data, TRUE);
																								if(isset($data_json["users"])){
																									if(isset($data_json["users"]["avatar"]) && $gdat->target_achieved < $mrow->activity_target){
																											$usprof = $this->db->select("IFNULL(avatar, '') as avatar")->get_where($this->table_motor, ["id"=>$motorist_id]);
																											if($usprof && $usprof->num_rows() > 0 && !empty($usprof->row()->avatar)){
																														$challog[date("Y-m-d H:i:s")] = ['target_achieved'=>1, 'avatar'=>$usprof->row()->avatar];
																														$this->db->update($this->table_detail, ["target_achieved"=>1, 'challenge_log'=>json_encode($challog)], ["id"=>$gdat->id]);
																														$this->db->insert("motorist_points", ["id"=>get_uuid(), "motorist_id"=>$motorist_id, "source"=>"challenge", "source_id"=>$mrow->id, "amount"=>$mrow->activity_point, "expired_at"=>date("Y-m-d H:i:s", strtotime("+1 year"))]);
																														setpush_notif($motorist_id, 'Keren, Anda berhasil menyelesaikan tantangan, poin anda bertambah '.$mrow->activity_point.' point', 'challenge', ['challenge'=>$mrow], 'Sukses melalui Tantangan', '', ['activity'=>'challenge', 'activity_id'=>$mrow->id]);
																											}
																									}
																								}
																								break;
															case 'warung-count'	:
															case 'warung'	: 	$data_json = json_decode($mrow->data, TRUE);
																								if(isset($data_json["warung"])){
																									if(isset($data_json["warung"]) && $gdat->target_achieved < $mrow->activity_target){
																											$usprof = $this->db->select("COUNT(id) as total")->get_where($this->table_warung, "input_by = '$motorist_id' AND created_at >= '".$gdat->created_at."'");
																											if($usprof && $usprof->num_rows() > 0 && isset($usprof->row()->total)){
																													if($usprof->row()->total <= $mrow->activity_target){
																															if(!empty($gdat->challenge_log)){
																																	$challog = json_decode($gdat->challenge_log, TRUE);
																																	$challog[date("Y-m-d H:i:s")] = ['target_achieved'=>$usprof->row()->total];
																															}else{
																																	$challog[date("Y-m-d H:i:s")] = ['target_achieved'=>$usprof->row()->total];
																															}
																															$this->db->update($this->table_detail, ["target_achieved"=>$usprof->row()->total, 'challenge_log'=>json_encode($challog)], ["id"=>$gdat->id]);
																															if($usprof->row()->total == $mrow->activity_target){
																																	$this->db->insert("motorist_points", ["id"=>get_uuid(), "motorist_id"=>$motorist_id, "source"=>"challenge", "source_id"=>$mrow->id, "amount"=>$mrow->activity_point, "expired_at"=>date("Y-m-d H:i:s", strtotime("+1 year"))]);
																																	setpush_notif($motorist_id, 'Keren, Anda berhasil menyelesaikan tantangan, poin anda bertambah '.$mrow->activity_point.' point', 'challenge', ['challenge'=>$mrow], 'Sukses melalui Tantangan', '', ['activity'=>'challenge', 'activity_id'=>$mrow->id]);
																															}
																													}else if($usprof->row()->total > $mrow->activity_target && $gdat->target_achieved < $mrow->activity_target){
																															if(!empty($gdat->challenge_log)){
																																	$challog = json_decode($gdat->challenge_log, TRUE);
																																	$challog[date("Y-m-d H:i:s")] = ['target_achieved'=>$usprof->row()->total];
																															}else{
																																	$challog[date("Y-m-d H:i:s")] = ['target_achieved'=>$usprof->row()->total];
																															}
																															$this->db->update($this->table_detail, ["target_achieved"=>$usprof->row()->total, 'challenge_log'=>json_encode($challog)], ["id"=>$gdat->id]);
																															$this->db->insert("motorist_points", ["id"=>get_uuid(), "motorist_id"=>$motorist_id, "source"=>"challenge", "source_id"=>$mrow->id, "amount"=>$mrow->activity_point, "expired_at"=>date("Y-m-d H:i:s", strtotime("+1 year"))]);
																															setpush_notif($motorist_id, 'Keren, Anda berhasil menyelesaikan tantangan, poin anda bertambah '.$mrow->activity_point.' point', 'challenge', ['challenge'=>$mrow], 'Sukses melalui Tantangan', '', ['activity'=>'challenge', 'activity_id'=>$mrow->id]);
																													}
																											}
																									}
																								}
																								break;
															case 'coupon-count'	: 	$data_json = json_decode($mrow->data, TRUE);
																								if(isset($data_json["coupon"])){
																									if(isset($data_json["coupon"]) && $gdat->target_achieved < $mrow->activity_target){
																											$usprof = $this->db->select("COUNT(id) as total")->get_where($this->table_coupon, "motorist_id = '$motorist_id' AND status = 5 AND succeeded_at >= '".$enrolldate."'");
																											if($usprof && $usprof->num_rows() > 0 && isset($usprof->row()->total)){
																													if($usprof->row()->total <= $mrow->activity_target){
																															if(!empty($gdat->challenge_log)){
																																	$challog = json_decode($gdat->challenge_log, TRUE);
																																	$challog[date("Y-m-d H:i:s")] = ['target_achieved'=>$usprof->row()->total];
																															}else{
																																	$challog[date("Y-m-d H:i:s")] = ['target_achieved'=>$usprof->row()->total];
																															}
																															$this->db->update($this->table_detail, ["target_achieved"=>$usprof->row()->total, 'challenge_log'=>json_encode($challog)], ["id"=>$gdat->id]);
																															if($usprof->row()->total == $mrow->activity_target){
																																	$this->db->insert("motorist_points", ["id"=>get_uuid(), "motorist_id"=>$motorist_id, "source"=>"challenge", "source_id"=>$mrow->id, "amount"=>$mrow->activity_point, "expired_at"=>date("Y-m-d H:i:s", strtotime("+1 year"))]);
																																	setpush_notif($motorist_id, 'Keren, Anda berhasil menyelesaikan tantangan, poin anda bertambah '.$mrow->activity_point.' point', 'challenge', ['challenge'=>$mrow], 'Sukses melalui Tantangan', '', ['activity'=>'challenge', 'activity_id'=>$mrow->id]);
																															}
																													}else if($usprof->row()->total > $mrow->activity_target && $gdat->target_achieved < $mrow->activity_target){
																															if(!empty($gdat->challenge_log)){
																																	$challog = json_decode($gdat->challenge_log, TRUE);
																																	$challog[date("Y-m-d H:i:s")] = ['target_achieved'=>$usprof->row()->total];
																															}else{
																																	$challog[date("Y-m-d H:i:s")] = ['target_achieved'=>$usprof->row()->total];
																															}
																															$this->db->update($this->table_detail, ["target_achieved"=>$usprof->row()->total, 'challenge_log'=>json_encode($challog)], ["id"=>$gdat->id]);
																															$this->db->insert("motorist_points", ["id"=>get_uuid(), "motorist_id"=>$motorist_id, "source"=>"challenge", "source_id"=>$mrow->id, "amount"=>$mrow->activity_point, "expired_at"=>date("Y-m-d H:i:s", strtotime("+1 year"))]);
																															setpush_notif($motorist_id, 'Keren, Anda berhasil menyelesaikan tantangan, poin anda bertambah '.$mrow->activity_point.' point', 'challenge', ['challenge'=>$mrow], 'Sukses melalui Tantangan', '', ['activity'=>'challenge', 'activity_id'=>$mrow->id]);
																													}
																											}
																									}
																								}
																								break;
															case 'coupon-product-total'	: 	$data_json = json_decode($mrow->data, TRUE);
																								if(isset($data_json["coupon"]["product"])){
																									if(isset($data_json["coupon"]["product"]) && is_array($data_json["coupon"]["product"]) && $gdat->target_achieved < $mrow->activity_target){
																											$data_prod = [];
																											foreach($data_json["coupon"]["product"] as $cprod){
																														$data_prod[] = "'$cprod'";
																											}
																											$improd = implode(",", $data_prod);
																											$usprof = $this->db->select("SUM(quantity) as total")->get_where($this->table_coupon_product, "product_id IN($improd) AND coupon_id IN(SELECT id FROM ".$this->table_coupon." WHERE motorist_id = '$motorist_id' AND status = 5 AND succeeded_at >= '".$enrolldate."')");
																											if($usprof && $usprof->num_rows() > 0 && isset($usprof->row()->total)){
																													if($usprof->row()->total <= $mrow->activity_target){
																															if(!empty($gdat->challenge_log)){
																																	$challog = json_decode($gdat->challenge_log, TRUE);
																																	$challog[date("Y-m-d H:i:s")] = ['target_achieved'=>$usprof->row()->total];
																															}else{
																																	$challog[date("Y-m-d H:i:s")] = ['target_achieved'=>$usprof->row()->total];
																															}
																															$this->db->update($this->table_detail, ["target_achieved"=>$usprof->row()->total, 'challenge_log'=>json_encode($challog)], ["id"=>$gdat->id]);
																															if($usprof->row()->total == $mrow->activity_target){
																																	$this->db->insert("motorist_points", ["id"=>get_uuid(), "motorist_id"=>$motorist_id, "source"=>"challenge", "source_id"=>$mrow->id, "amount"=>$mrow->activity_point, "expired_at"=>date("Y-m-d H:i:s", strtotime("+1 year"))]);
																																	setpush_notif($motorist_id, 'Keren, Anda berhasil menyelesaikan tantangan, poin anda bertambah '.$mrow->activity_point.' point', 'challenge', ['challenge'=>$mrow], 'Sukses melalui Tantangan', '', ['activity'=>'challenge', 'activity_id'=>$mrow->id]);
																															}
																													}else if($usprof->row()->total > $mrow->activity_target && $gdat->target_achieved < $mrow->activity_target){
																															if(!empty($gdat->challenge_log)){
																																	$challog = json_decode($gdat->challenge_log, TRUE);
																																	$challog[date("Y-m-d H:i:s")] = ['target_achieved'=>$usprof->row()->total];
																															}else{
																																	$challog[date("Y-m-d H:i:s")] = ['target_achieved'=>$usprof->row()->total];
																															}
																															$this->db->update($this->table_detail, ["target_achieved"=>$usprof->row()->total, 'challenge_log'=>json_encode($challog)], ["id"=>$gdat->id]);
																															$this->db->insert("motorist_points", ["id"=>get_uuid(), "motorist_id"=>$motorist_id, "source"=>"challenge", "source_id"=>$mrow->id, "amount"=>$mrow->activity_point, "expired_at"=>date("Y-m-d H:i:s", strtotime("+1 year"))]);
																															setpush_notif($motorist_id, 'Keren, Anda berhasil menyelesaikan tantangan, poin anda bertambah '.$mrow->activity_point.' point', 'challenge', ['challenge'=>$mrow], 'Sukses melalui Tantangan', '', ['activity'=>'challenge', 'activity_id'=>$mrow->id]);
																													}
																											}
																									}
																								}
																								break;
															case 'coupon-unique-count'	: 	$data_json = json_decode($mrow->data, TRUE);
																								if(isset($data_json["coupon"])){
																										if(isset($data_json["coupon"]) && $gdat->target_achieved < $mrow->activity_target){
																												$usprof = $this->db->group_by("grosir_id")->select("SUM(total_price) as total")->get_where($this->table_coupon, "motorist_id = '$motorist_id' AND status = 5 AND succeeded_at >= '".$enrolldate."'");
																												if($usprof && $usprof->num_rows() > 0){
																														if($usprof->num_rows() <= $mrow->activity_target){
																																if(!empty($gdat->challenge_log)){
																																		$challog = json_decode($gdat->challenge_log, TRUE);
																																		$challog[date("Y-m-d H:i:s")] = ['target_achieved'=>$usprof->num_rows()];
																																}else{
																																		$challog[date("Y-m-d H:i:s")] = ['target_achieved'=>$usprof->num_rows()];
																																}
																																$this->db->update($this->table_detail, ["target_achieved"=>$usprof->num_rows(), 'challenge_log'=>json_encode($challog)], ["id"=>$gdat->id]);
																																if($usprof->num_rows() == $mrow->activity_target){
																																		$this->db->insert("motorist_points", ["id"=>get_uuid(), "motorist_id"=>$motorist_id, "source"=>"challenge", "source_id"=>$mrow->id, "amount"=>$mrow->activity_point, "expired_at"=>date("Y-m-d H:i:s", strtotime("+1 year"))]);
																																		setpush_notif($motorist_id, 'Keren, Anda berhasil menyelesaikan tantangan, poin anda bertambah '.$mrow->activity_point.' point', 'challenge', ['challenge'=>$mrow], 'Sukses melalui Tantangan', '', ['activity'=>'challenge', 'activity_id'=>$mrow->id]);
																																}
																														}else if($usprof->num_rows() > $mrow->activity_target && $gdat->target_achieved < $mrow->activity_target){
																																if(!empty($gdat->challenge_log)){
																																		$challog = json_decode($gdat->challenge_log, TRUE);
																																		$challog[date("Y-m-d H:i:s")] = ['target_achieved'=>$usprof->num_rows()];
																																}else{
																																		$challog[date("Y-m-d H:i:s")] = ['target_achieved'=>$usprof->num_rows()];
																																}
																																$this->db->update($this->table_detail, ["target_achieved"=>$usprof->num_rows(), 'challenge_log'=>json_encode($challog)], ["id"=>$gdat->id]);
																																$this->db->insert("motorist_points", ["id"=>get_uuid(), "motorist_id"=>$motorist_id, "source"=>"challenge", "source_id"=>$mrow->id, "amount"=>$mrow->activity_point, "expired_at"=>date("Y-m-d H:i:s", strtotime("+1 year"))]);
																																setpush_notif($motorist_id, 'Keren, Anda berhasil menyelesaikan tantangan, poin anda bertambah '.$mrow->activity_point.' point', 'challenge', ['challenge'=>$mrow], 'Sukses melalui Tantangan', '', ['activity'=>'challenge', 'activity_id'=>$mrow->id]);
																														}
																												}
																										}
																								}
																								break;
															case 'coupon-unique-total'	: 	$data_json = json_decode($mrow->data, TRUE);
																								if(isset($data_json["coupon"])){
																										if(isset($data_json["coupon"]) && $gdat->target_achieved < $mrow->activity_target){
																												$usprof = $this->db->group_by("grosir_id")->select("IFNULL(SUM(total_price), 0) as total")->get_where($this->table_coupon, "motorist_id = '$motorist_id' AND status = 5 AND succeeded_at >= '".$enrolldate."'");
																												if($usprof && $usprof->num_rows() > 0){
																														$totale = 0;
																														foreach($usprof->result() as $uspr){
																																$totale += $uspr->total;
																														}
																														if($totale <= $mrow->activity_target){
																																if(!empty($gdat->challenge_log)){
																																		$challog = json_decode($gdat->challenge_log, TRUE);
																																		$challog[date("Y-m-d H:i:s")] = ['target_achieved'=>$totale];
																																}else{
																																		$challog[date("Y-m-d H:i:s")] = ['target_achieved'=>$totale];
																																}
																																$this->db->update($this->table_detail, ["target_achieved"=>$totale, 'challenge_log'=>json_encode($challog)], ["id"=>$gdat->id]);
																																if($totale == $mrow->activity_target){
																																		$this->db->insert("motorist_points", ["id"=>get_uuid(), "motorist_id"=>$motorist_id, "source"=>"challenge", "source_id"=>$mrow->id, "amount"=>$mrow->activity_point, "expired_at"=>date("Y-m-d H:i:s", strtotime("+1 year"))]);
																																		setpush_notif($motorist_id, 'Keren, Anda berhasil menyelesaikan tantangan, poin anda bertambah '.$mrow->activity_point.' point', 'challenge', ['challenge'=>$mrow], 'Sukses melalui Tantangan', '', ['activity'=>'challenge', 'activity_id'=>$mrow->id]);
																																}
																														}else if($totale > $mrow->activity_target && $gdat->target_achieved < $mrow->activity_target){
																																if(!empty($gdat->challenge_log)){
																																		$challog = json_decode($gdat->challenge_log, TRUE);
																																		$challog[date("Y-m-d H:i:s")] = ['target_achieved'=>$totale];
																																}else{
																																		$challog[date("Y-m-d H:i:s")] = ['target_achieved'=>$totale];
																																}
																																$this->db->update($this->table_detail, ["target_achieved"=>$totale, 'challenge_log'=>json_encode($challog)], ["id"=>$gdat->id]);
																																$this->db->insert("motorist_points", ["id"=>get_uuid(), "motorist_id"=>$motorist_id, "source"=>"challenge", "source_id"=>$mrow->id, "amount"=>$mrow->activity_point, "expired_at"=>date("Y-m-d H:i:s", strtotime("+1 year"))]);
																																setpush_notif($motorist_id, 'Keren, Anda berhasil menyelesaikan tantangan, poin anda bertambah '.$mrow->activity_point.' point', 'challenge', ['challenge'=>$mrow], 'Sukses melalui Tantangan', '', ['activity'=>'challenge', 'activity_id'=>$mrow->id]);
																														}
																												}
																										}
																								}
																								break;
															case 'coupon-total'	: 	$data_json = json_decode($mrow->data, TRUE);
																								if(isset($data_json["coupon"])){
																									if(isset($data_json["coupon"]) && $gdat->target_achieved < $mrow->activity_target){
																											$usprof = $this->db->select("IFNULL(SUM(total_price), 0) as total")->get_where($this->table_coupon, "motorist_id = '$motorist_id' AND status = 5 AND succeeded_at >= '".$enrolldate."'");
																											if($usprof && $usprof->num_rows() > 0 && isset($usprof->row()->total)){
																													if($usprof->row()->total <= $mrow->activity_target){
																															if(!empty($gdat->challenge_log)){
																																	$challog = json_decode($gdat->challenge_log, TRUE);
																																	$challog[date("Y-m-d H:i:s")] = ['target_achieved'=>$usprof->row()->total];
																															}else{
																																	$challog[date("Y-m-d H:i:s")] = ['target_achieved'=>$usprof->row()->total];
																															}
																															$this->db->update($this->table_detail, ["target_achieved"=>$usprof->row()->total, 'challenge_log'=>json_encode($challog)], ["id"=>$gdat->id]);
																															if($usprof->row()->total == $mrow->activity_target){
																																	$this->db->insert("motorist_points", ["id"=>get_uuid(), "motorist_id"=>$motorist_id, "source"=>"challenge", "source_id"=>$mrow->id, "amount"=>$mrow->activity_point, "expired_at"=>date("Y-m-d H:i:s", strtotime("+1 year"))]);
																																	setpush_notif($motorist_id, 'Keren, Anda berhasil menyelesaikan tantangan, poin anda bertambah '.$mrow->activity_point.' point', 'challenge', ['challenge'=>$mrow], 'Sukses melalui Tantangan', '', ['activity'=>'challenge', 'activity_id'=>$mrow->id]);
																															}
																													}else if($usprof->row()->total > $mrow->activity_target && $gdat->target_achieved < $mrow->activity_target){
																															if(!empty($gdat->challenge_log)){
																																	$challog = json_decode($gdat->challenge_log, TRUE);
																																	$challog[date("Y-m-d H:i:s")] = ['target_achieved'=>$usprof->row()->total];
																															}else{
																																	$challog[date("Y-m-d H:i:s")] = ['target_achieved'=>$usprof->row()->total];
																															}
																															$this->db->update($this->table_detail, ["target_achieved"=>$usprof->row()->total, 'challenge_log'=>json_encode($challog)], ["id"=>$gdat->id]);
																															$this->db->insert("motorist_points", ["id"=>get_uuid(), "motorist_id"=>$motorist_id, "source"=>"challenge", "source_id"=>$mrow->id, "amount"=>$mrow->activity_point, "expired_at"=>date("Y-m-d H:i:s", strtotime("+1 year"))]);
																															setpush_notif($motorist_id, 'Keren, Anda berhasil menyelesaikan tantangan, poin anda bertambah '.$mrow->activity_point.' point', 'challenge', ['challenge'=>$mrow], 'Sukses melalui Tantangan', '', ['activity'=>'challenge', 'activity_id'=>$mrow->id]);
																													}
																											}
																									}
																								}
																								break;
															case 'sales' :
															case 'sales-total'	: 	$data_json = json_decode($mrow->data, TRUE);
																								if(isset($data_json["sales"])){
																									if(isset($data_json["sales"]) && $gdat->target_achieved < $mrow->activity_target){
																											$usprof = $this->db->select("IFNULL(SUM(subtotal), 0) as total")->get_where($this->table_sales, "input_by = '$motorist_id' AND created_at >= '".$gdat->created_at."'");
																											if($usprof && $usprof->num_rows() > 0 && isset($usprof->row()->total)){
																													if($usprof->row()->total <= $mrow->activity_target){
																															if(!empty($gdat->challenge_log)){
																																	$challog = json_decode($gdat->challenge_log, TRUE);
																																	$challog[date("Y-m-d H:i:s")] = ['target_achieved'=>$usprof->row()->total];
																															}else{
																																	$challog[date("Y-m-d H:i:s")] = ['target_achieved'=>$usprof->row()->total];
																															}
																															$this->db->update($this->table_detail, ["target_achieved"=>$usprof->row()->total, 'challenge_log'=>json_encode($challog)], ["id"=>$gdat->id]);
																															if($usprof->row()->total == $mrow->activity_target){
																																	$this->db->insert("motorist_points", ["id"=>get_uuid(), "motorist_id"=>$motorist_id, "source"=>"challenge", "source_id"=>$mrow->id, "amount"=>$mrow->activity_point, "expired_at"=>date("Y-m-d H:i:s", strtotime("+1 year"))]);
																																	setpush_notif($motorist_id, 'Keren, Anda berhasil menyelesaikan tantangan, poin anda bertambah '.$mrow->activity_point.' point', 'challenge', ['challenge'=>$mrow], 'Sukses melalui Tantangan', '', ['activity'=>'challenge', 'activity_id'=>$mrow->id]);
																															}
																													}else if($usprof->row()->total > $mrow->activity_target && $gdat->target_achieved < $mrow->activity_target){
																															if(!empty($gdat->challenge_log)){
																																	$challog = json_decode($gdat->challenge_log, TRUE);
																																	$challog[date("Y-m-d H:i:s")] = ['target_achieved'=>$usprof->row()->total];
																															}else{
																																	$challog[date("Y-m-d H:i:s")] = ['target_achieved'=>$usprof->row()->total];
																															}
																															$this->db->update($this->table_detail, ["target_achieved"=>$usprof->row()->total, 'challenge_log'=>json_encode($challog)], ["id"=>$gdat->id]);
																															$this->db->insert("motorist_points", ["id"=>get_uuid(), "motorist_id"=>$motorist_id, "source"=>"challenge", "source_id"=>$mrow->id, "amount"=>$mrow->activity_point, "expired_at"=>date("Y-m-d H:i:s", strtotime("+1 year"))]);
																															setpush_notif($motorist_id, 'Keren, Anda berhasil menyelesaikan tantangan, poin anda bertambah '.$mrow->activity_point.' point', 'challenge', ['challenge'=>$mrow], 'Sukses melalui Tantangan', '', ['activity'=>'challenge', 'activity_id'=>$mrow->id]);
																													}
																											}
																									}
																								}
																								break;
															default :					$data_json = json_decode($mrow->data, TRUE);
																								if(isset($data_json["users"])){
																									if(isset($data_json["users"]["avatar"]) && $gdat->target_achieved < $mrow->activity_target){
																											$usprof = $this->db->select("IFNULL(avatar, '') as avatar")->get_where($this->table_motor, ["id"=>$motorist_id]);
																											if($usprof && $usprof->num_rows() > 0 && !empty($usprof->row()->avatar)){
																														$challog[date("Y-m-d H:i:s")] = ['target_achieved'=>1, 'avatar'=>$usprof->row()->avatar];
																														$this->db->update($this->table_detail, ["target_achieved"=>1, 'challenge_log'=>json_encode($challog)], ["id"=>$gdat->id]);
																														$this->db->insert("motorist_points", ["id"=>get_uuid(), "motorist_id"=>$motorist_id, "source"=>"challenge", "source_id"=>$mrow->id, "amount"=>$mrow->activity_point, "expired_at"=>date("Y-m-d H:i:s", strtotime("+1 year"))]);
																														setpush_notif($motorist_id, 'Keren, Anda berhasil menyelesaikan tantangan, poin anda bertambah '.$mrow->activity_point.' point', 'challenge', ['challenge'=>$mrow], 'Sukses melalui Tantangan', '', ['activity'=>'challenge', 'activity_id'=>$mrow->id]);
																											}
																									}
																								}
																								break;
													}
											}
									}
							}
					}
			}

			function findCond($where, $select='') {
					if(!empty($select)) $this->db->select($select);
					$usdata = $this->db->get_where($this->table, $where);
					return $usdata->row();
			}
}
