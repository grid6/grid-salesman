<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class M_Sales extends CI_Model{

			protected $table;
			protected $table_warung;

			function __construct(){
					parent::__construct();
					$this->table = 'warung_sales';
					$this->table_warung = 'warungs';
			}

			function insert($data){
					if(!isset($data["id"])) $data["id"] = get_uuid();
					$this->db->insert($this->table, $data);
					return $data["id"];
			}

			function update($data, $cond){
					if(!is_array($cond)){
							$cond = array('id'=>$cond);
					}
					return $this->db->update($this->table, $data, $cond);
			}

			function delete($cond){
					if(!is_array($cond)){
							$cond = array('id'=>$cond);
					}
					return $this->db->delete($this->table, $cond);
			}

			function find($id) {
					$usdata = $this->db->get_where($this->table, array("id"=>$id));
					$rowdata = ($usdata && $usdata->num_rows() > 0) ? $usdata->row() : null;
					return $rowdata;
			}

			function findCond($where, $select='') {
					if(!empty($select)) $this->db->select($select);
					$usdata = $this->db->get_where($this->table, $where);
					return $usdata->row();
			}

			function findConds($where, $select='', $rpp=20, $page=1) {
					$spage = ($page > 1) ? ($page - 1) * $rpp : 0;
					if(!empty($rpp) && $rpp != 'all') $this->db->limit($rpp, $spage);
					if(!empty($select)) $this->db->select($select);
					$usdata = $this->db->get_where($this->table, $where);
					$sodata = ($usdata && $usdata->num_rows() > 0) ? $usdata->result() : [];
					return $sodata;
			}
}
