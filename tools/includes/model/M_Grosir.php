<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class M_Grosir extends CI_Model{

			protected $table;
			protected $table_motor;
			protected $table_promo;
			protected $table_voucher;
			protected $table_banner;

			function __construct(){
					parent::__construct();
					$this->table = 'grosirs';
					$this->table_promo  = 'promos';
					$this->table_voucher = 'coupons';
					$this->table_banner = 'grosir_banner';
					$this->load->model('M_Motorist', 'motor');
			}

			function insert($data){
					if(!isset($data["id"])) $data["id"] = get_uuid();
					$this->db->insert($this->table, $data);
					return $data["id"];
			}

			function update($data, $cond){
					if(!is_array($cond)){
							$cond = array('id'=>$cond);
					}
					return $this->db->update($this->table, $data, $cond);
			}

			function delete($cond){
					if(!is_array($cond)){
							$cond = array('id'=>$cond);
					}
					return $this->db->delete($this->table, $cond);
			}

			function findCond($where, $select='') {
					if(!empty($select)) $this->db->select($select);
					$usdata = $this->db->get_where($this->table, $where);
					return $usdata->row();
			}

			function find($where, $select='') {
					if(!empty($select)) $this->db->select($select);
					if(!is_array($where)){
							$where = array('id'=>$where);
					}
					$usdata = $this->db->get_where($this->table, $where);
					return $usdata->row();
			}

			function lists($cond=null, $select='', $rpp=20, $page=1){
					$spage = ($page > 1) ? ($page - 1) * $rpp : 0;
					if(!empty($rpp) && $rpp != 'all') $this->db->limit($rpp, $spage);
					if(!empty($cond)) $this->db->where($cond);
					if(!empty($select)) $this->db->select($select);
					$rsdata = $this->db->get($this->table);
					$data_ret = ($rsdata && $rsdata->num_rows() > 0) ? $rsdata->result() : [];
					return $data_ret;
			}

			function totaldata($cond=null){
					if(!empty($cond)) $this->db->where($cond);
					$rsdata = $this->db->select("COUNT(id) as total")->get($this->table);
					$tot = ($rsdata && $rsdata->num_rows() > 0) ? (int)$rsdata->row()->total : 0;
					return $tot;
			}

			function listjoined($cond=null, $select='', $rpp=20, $page=1){
					$spage = ($page > 1) ? ($page - 1) * $rpp : 0;
					if(!empty($rpp) && $rpp != 'all') $this->db->limit($rpp, $spage);
					if(!empty($cond)) $this->db->where($cond);
					if(!empty($select)) $this->db->select($select);
					$this->db->order_by($this->table_promo.".end_date", "ASC");
					$this->db->group_by($this->table_promo.".grosir_id");
					$this->db->join($this->table_promo, $this->table_promo.".grosir_id = ".$this->table.".id");
					$rsdata = $this->db->get($this->table);
					$data_ret = ($rsdata && $rsdata->num_rows() > 0) ? $rsdata->result() : [];
					return $data_ret;
			}

			function totaljoined($cond=null){
					if(!empty($cond)) $this->db->where($cond);
					$this->db->group_by($this->table_promo.".grosir_id");
					$this->db->join($this->table_promo, $this->table_promo.".grosir_id = ".$this->table.".id");
					$rsdata = $this->db->select('COUNT(grosir_id) as total')->get($this->table);
					$tot = ($rsdata && $rsdata->num_rows() > 0) ? (int)$rsdata->row()->total : 0;
					return $tot;
			}

			function listBought($cond=null, $select='', $rpp=20, $page=1){
					$spage = ($page > 1) ? ($page - 1) * $rpp : 0;
					if(!empty($rpp) && $rpp != 'all') $this->db->limit($rpp, $spage);
					if(!empty($cond)) $this->db->where($cond);
					if(!empty($select)) $this->db->select($select);
					$this->db->group_by($this->table_voucher.".`grosir_id`");
					$this->db->join($this->table_voucher, "`".$this->table_voucher."`.`grosir_id` = `".$this->table."`.`id`");
					$rsdata = $this->db->get($this->table);
					$data_ret = ($rsdata && $rsdata->num_rows() > 0) ? $rsdata->result() : [];
					return $data_ret;
			}

			function totalBought($cond=null){
					if(!empty($cond)) $this->db->where($cond);
					$this->db->group_by($this->table_voucher.".`grosir_id`");
					$this->db->join($this->table_voucher, "`".$this->table_voucher."`.`grosir_id` = `".$this->table."`.`id`");
					$rsdata = $this->db->select("COUNT(".$this->table.".id) as total")->get($this->table);
					$data_ret = ($rsdata && $rsdata->num_rows() > 0) ? (int)$rsdata->row()->total : 0;
					return $data_ret;
			}

			function getBanner($cond=null, $select='', $rpp=20, $page=1){
					$spage = ($page > 1) ? ($page - 1) * $rpp : 0;
					if(!empty($rpp) && $rpp != 'all') $this->db->limit($rpp, $spage);
					if(!empty($cond)) $this->db->where($cond);
					if(!empty($select)) $this->db->select($select);
					$rsdata = $this->db->get($this->table_banner);
					$data_ret = ($rsdata && $rsdata->num_rows() > 0) ? $rsdata->result_array() : [];
					return $data_ret;
			}

			function listByArea($uid=0, $rpp=20, $page=1, $name=""){
					$data_ret = [];
					if(!empty($uid)){
							$motor = $this->motor->findCond(["id"=>$uid]);
							if(isset($motor->id)){
									$whereAdmo = (!empty($name)) ? " AND name LIKE '%$name%'" : "";
									$latlong = $motor->last_location;
									if(!empty($latlong)){
											$lalo = explode(",", $latlong);
											$latitude = $lalo[0];
											$longitude = (isset($lalo[1])) ? $lalo[1] : '';
											if(!empty($longitude)){
													$daret = $disid = [];
													$data_loc = get_location($latitude, $longitude);
													if(isset($data_loc["city"])){
															$citcit = $data_loc["city"];
															$cits = $this->db->select("city_code as id")->get_where("cities", "city_name LIKE '%$citcit%'");
															if($cits && $cits->num_rows() > 0){
																	foreach($cits->result() as $cit){
																			$city_id = $cit->id;
																			$whereadj = "";
																			$gros2 = $this->listjoined("promos.status = 'ACTIVE' AND city_code = '$city_id'$whereadj".$whereAdmo, $this->table.".id, name, IFNULL(latitude, '') as latitude, IFNULL(longitude, '') as longitude, IFNULL(address, '') as address, IFNULL(avatar, '') as avatar, province_code as province_id, district_code as district_id, city_code as city_id, subdistrict_code as subdistrict_id, area_service_id as area_service", $rpp, $page);
																			foreach($gros2 as $gr2){
																					$distanz = (!empty($latitude) && !empty($longitude) && !empty($gr2->latitude) && !empty($gr2->longitude)) ? calculate_distance($latitude, $longitude, $gr2->latitude, $gr2->longitude) : '';
																					$gr2->distance = $distanz;
																					$daret[] = $gr2;
																			}
																	}
															}
													}
													array_multisort( array_column($daret, "distance"), SORT_ASC, $daret );
													$data_ret = $daret;
											}
									}else{

											$daret = $disid = [];

											if(!empty($motor->area_operational)){
													$area = $motor->area_operational;
													$gros = $this->listjoined("promos.status = 'ACTIVE' AND area_service_id = '$area'$whereAdmo", $this->table.".id, name, IFNULL(latitude, '') as latitude, IFNULL(longitude, '') as longitude, IFNULL(avatar, '') as avatar, IFNULL(address, '') as address, province_code as province_id, district_code as district_id, city_code as city_id, subdistrict_code as subdistrict_id, area_service_id as area_service", $rpp, $page);
													foreach($gros as $gr){
															$disid[] = "'".$gr->id."'";
															$gr->distance = '';
															$daret[] = $gr;
													}
											}

											$data_ret = $daret;
									}
							}
					}
					return $data_ret;
			}

			function onlyListByArea($uid=0, $rpp=20, $page=1, $name=""){
					$data_ret = [];
					if(!empty($uid)){
							$motor = $this->motor->findCond(["id"=>$uid]);
							if(isset($motor->id)){
									$whereAdmo = (!empty($name)) ? " AND name LIKE '%$name%'" : "";
									$latlong = $motor->last_location;
									if(!empty($latlong)){
											$lalo = explode(",", $latlong);
											$latitude = $lalo[0];
											$longitude = (isset($lalo[1])) ? $lalo[1] : '';
											if(!empty($longitude)){
													$daret = $disid = [];
													$data_loc = get_location($latitude, $longitude);
													if(isset($data_loc["city"]) && count($daret) < 5){
															$citcit = $data_loc["city"];
															$cits = $this->db->select("city_code as id")->get_where("cities", "city_name LIKE '%$citcit%'");
															if($cits && $cits->num_rows() > 0){
																	foreach($cits->result() as $cit){
																			$city_id = $cit->id;
																			$whereadj = "";
																			$gros2 = $this->lists("city_code = '$city_id'$whereadj".$whereAdmo, "id, name, IFNULL(latitude, '') as latitude, IFNULL(longitude, '') as longitude, IFNULL(address, '') as address, IFNULL(avatar, '') as avatar, province_code as province_id, district_code as district_id, city_code as city_id, subdistrict_code as subdistrict_id, area_service_id as area_service", $rpp, $page);
																			foreach($gros2 as $gr2){
																					$distanz = (!empty($latitude) && !empty($longitude) && !empty($gr2->latitude) && !empty($gr2->longitude)) ? calculate_distance($latitude, $longitude, $gr2->latitude, $gr2->longitude) : '';
																					$gr2->distance = $distanz;
																					$daret[] = $gr2;
																			}
																	}
															}
													}
													array_multisort( array_column($daret, "distance"), SORT_ASC, $daret );
													$data_ret = $daret;
											}
									}else{

											$daret = $disid = [];

											if(!empty($motor->area_operational)){
													$area = $motor->area_operational;
													$gros = $this->lists("area_service_id = '$area'$whereAdmo", "id, name, IFNULL(latitude, '') as latitude, IFNULL(longitude, '') as longitude, IFNULL(address, '') as address, IFNULL(avatar, '') as avatar, IFNULL(avatar, '') as avatar, province_code as province_id, district_code as district_id, city_code as city_id, subdistrict_code as subdistrict_id, area_service_id as area_service", $rpp, $page);
													foreach($gros as $gr){
															$disid[] = "'".$gr->id."'";
															$gr->distance = '';
															$daret[] = $gr;
													}
											}

											$data_ret = $daret;
									}
							}
					}
					return $data_ret;
			}

			function listByAreaMeta($uid=0, $rpp=20, $page=1, $name=""){
					$data_ret = [];
					$total_join = 0;
					if(!empty($uid)){
							$motor = $this->motor->findCond(["id"=>$uid]);
							if(isset($motor->id)){
									$whereAdmo = (!empty($name)) ? " AND name LIKE '%$name%'" : "";
									$latlong = $motor->last_location;
									if(!empty($latlong)){
											$lalo = explode(",", $latlong);
											$latitude = $lalo[0];
											$longitude = (isset($lalo[1])) ? $lalo[1] : '';
											if(!empty($longitude)){
													$daret = $disid = [];
													$data_loc = get_location($latitude, $longitude);
													if(isset($data_loc["city"])){
															$citcit = $data_loc["city"];
															$cits = $this->db->select("city_code as id")->get_where("cities", "city_name LIKE '%$citcit%'");
															if($cits && $cits->num_rows() > 0){
																	foreach($cits->result() as $cit){
																			$city_id = $cit->id;
																			$totjoin = $this->totaljoined("promos.status = 'ACTIVE' AND city_code = '$city_id'$whereAdmo");
																			$total_join += $totjoin;
																			$gros2 = $this->listjoined("promos.status = 'ACTIVE' AND city_code = '$city_id'$whereAdmo", $this->table.".id, name, IFNULL(latitude, '') as latitude, IFNULL(longitude, '') as longitude, IFNULL(avatar, '') as avatar, IFNULL(address, '') as address, province_code as province_id, district_code as district_id, city_code as city_id, subdistrict_code as subdistrict_id, area_service_id as area_service", $rpp, $page);
																			foreach($gros2 as $gr2){
																					$distanz = (!empty($latitude) && !empty($longitude) && !empty($gr2->latitude) && !empty($gr2->longitude)) ? calculate_distance($latitude, $longitude, $gr2->latitude, $gr2->longitude) : '';
																					$gr2->distance = $distanz;
																					$daret[] = $gr2;
																			}
																	}
															}
													}
													array_multisort( array_column($daret, "distance"), SORT_ASC, $daret );
													$data_ret = $daret;
											}
									}else{

											$daret = $disid = [];

											if(!empty($motor->area_operational)){
													$area = $motor->area_operational;
													$totjoin = $this->totaljoined("promos.status = 'ACTIVE' AND area_service_id = '$area'$whereAdmo");
													$gros = $this->listjoined("promos.status = 'ACTIVE' AND area_service_id = '$area'$whereAdmo", $this->table.".id, name, IFNULL(latitude, '') as latitude, IFNULL(longitude, '') as longitude, IFNULL(avatar, '') as avatar, IFNULL(address, '') as address, province_code as province_id, district_code as district_id, city_code as city_id, subdistrict_code as subdistrict_id, area_service_id as area_service", $rpp, $page);
													foreach($gros as $gr){
															$disid[] = "'".$gr->id."'";
															$gr->distance = '';
															$daret[] = $gr;
													}
											}

											$data_ret = $daret;
									}
							}
					}

					$tot_page = ($total_join > 1) ? ceil($total_join / $rpp) : 1;
					$prev_page = ($page > 1) ? $page - 1 : 1;
					$next_page = ($page >= $tot_page) ? $tot_page : ($page + 1);

					$meta_ret = [
						'rpp'=>$rpp,
						'total_page'=>$tot_page,
						'current_page'=>$page,
						'prev_page'=>$prev_page,
						'next_page'=>$next_page,
						'total_data'=>$total_join
					];
					$resp_ret = ['data'=>$data_ret, 'meta'=>$meta_ret];
					return $resp_ret;
			}

			function onlyListByAreaMeta($uid=0, $rpp=20, $page=1, $name=""){
					$data_ret = [];
					$total_join = 0;
					if(!empty($uid)){
							$motor = $this->motor->findCond(["id"=>$uid]);
							if(isset($motor->id)){
									$whereAdmo = (!empty($name)) ? " AND name LIKE '%$name%'" : "";
									$latlong = $motor->last_location;
									if(!empty($latlong)){
											$lalo = explode(",", $latlong);
											$latitude = $lalo[0];
											$longitude = (isset($lalo[1])) ? $lalo[1] : '';
											if(!empty($longitude)){
													$daret = $disid = [];
													$data_loc = get_location($latitude, $longitude);
													if(isset($data_loc["city"])){
															$citcit = $data_loc["city"];
															$cits = $this->db->select("city_code as id")->get_where("cities", "city_name LIKE '%$citcit%'");
															if($cits && $cits->num_rows() > 0){
																	foreach($cits->result() as $cit){
																			$city_id = $cit->id;
																			$totjoin = $this->totaldata("city_code = '$city_id'$whereAdmo");
																			$total_join += $totjoin;
																			$gros2 = $this->lists("city_code = '$city_id'$whereAdmo", "id, name, IFNULL(latitude, '') as latitude, IFNULL(avatar, '') as avatar, IFNULL(longitude, '') as longitude, IFNULL(address, '') as address, province_code as province_id, district_code as district_id, city_code as city_id, subdistrict_code as subdistrict_id, area_service_id as area_service", $rpp, $page);
																			foreach($gros2 as $gr2){
																					$distanz = (!empty($latitude) && !empty($longitude) && !empty($gr2->latitude) && !empty($gr2->longitude)) ? calculate_distance($latitude, $longitude, $gr2->latitude, $gr2->longitude) : '';
																					$gr2->distance = $distanz;
																					$daret[] = $gr2;
																			}
																	}
															}
													}
													array_multisort( array_column($daret, "distance"), SORT_ASC, $daret );
													$data_ret = $daret;
											}
									}else{

											$daret = $disid = [];

											if(!empty($motor->area_operational)){
													$area = $motor->area_operational;
													$totjoin = $this->totaldata("area_service_id = '$area'$whereAdmo");
													$total_join = $totjoin;
													$gros = $this->lists("area_service_id = '$area'$whereAdmo", "id, name, IFNULL(latitude, '') as latitude, IFNULL(longitude, '') as longitude, IFNULL(address, '') as address, IFNULL(avatar, '') as avatar, province_code as province_id, district_code as district_id, city_code as city_id, subdistrict_code as subdistrict_id, area_service_id as area_service", $rpp, $page);
													foreach($gros as $gr){
															$disid[] = "'".$gr->id."'";
															$gr->distance = '';
															$daret[] = $gr;
													}
											}

											$data_ret = $daret;
									}
							}
					}

					$tot_page = ($total_join > 1) ? ceil($total_join / $rpp) : 1;
					$prev_page = ($page > 1) ? $page - 1 : 1;
					$next_page = ($page >= $tot_page) ? $tot_page : ($page + 1);

					$meta_ret = [
							'rpp'=>$rpp,
							'total_page'=>$tot_page,
							'current_page'=>$page,
							'prev_page'=>$prev_page,
							'next_page'=>$next_page,
							'total_data'=>$total_join
					];
					$resp_ret = ['data'=>$data_ret, 'meta'=>$meta_ret];
					return $resp_ret;
			}

			function listByCity($city_id=0, $rpp=20, $page=1, $name=""){
						$data_ret = [];
						$whereAdmo = (!empty($name)) ? " AND name LIKE '%$name%'" : "";
						$daret = $disid = [];
						if(!empty($city_id)){
									$gros2 = $this->lists("id IN (SELECT grosir_id FROM promos WHERE status = 'ACTIVE') AND city_code = '$city_id'".$whereAdmo, "id, name, IFNULL(latitude, '') as latitude, IFNULL(longitude, '') as longitude, IFNULL(address, '') as address, province_code as province_id, district_code as district_id, city_code as city_id, subdistrict_code as subdistrict_id, area_service_id as area_service", $rpp, $page);
									foreach($gros2 as $gr2){
												$gr2->distance = '';
												$daret[] = $gr2;
									}
						}
						$data_ret = $daret;
						return $data_ret;
			}
}
