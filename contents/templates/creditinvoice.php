<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="x-apple-disable-message-reformatting">
    <title>Invoice</title>
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@200;300;400;600;700;800;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?= base_url("assets") ?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?= base_url("assets") ?>/css/style.css">
</head>
<body>
    <header>
        <!-- <div class="header-nav">
            <a href="form-add-customer.html" class="d-flex align-items-center z-index-1"><img src="<?= base_url("assets") ?>/images/arrow-icon.svg" alt=""/></a>
            <h4 class="position-absolute text-center start-0 end-0 m-0">Select Package</h4>
        </div> -->
    </header>
    <main>
        <div class="container">
            <div class="card-body p-0 mb-5">
                <div class="account-card justify-content-center border-0">
                    <!-- <div class="avatar">KL</div> -->
                    <div class="d-flex flex-column justify-content-center align-items-center gap-1 text-white">
                        <h5 class="m-0"><?= $cust->name ?></h5>
                        <p class="m-0"><?= $cust->email ?></p>
                    </div>
                </div>
                <div style="display:block;margin:15px 0;height:1px;background: rgb(2,181,211);background: linear-gradient(90deg, rgba(2,181,211,1) 0%, rgba(230,240,205,1) 100%);">&nbsp;</div>
                <div class="text-center">
                    <p>Amount Due</p>
                    <?php
                    $totalPaid = $plan->price;
                    if(isset($trans['data']['fee'])) $totalPaid += $trans['data']['fee'];
                    ?>
                    <div class="h1 fw-bold text-primary">Rp<?= number_format($totalPaid, 0, ",", ".") ?></div>
                    <?php
                    switch($trans["data"]["channel"]){
                        case "CASH" : $channel="Cash";break;
                        case "BC" : $channel="Virtual Account BCA";break;
                        case "BR" : $channel="Virtual Account BRI";break;
                        case "NC" : $channel="Virtual Account BNC";break;
                        case "NQ" : $channel="Virtual Account Nobu QRIS";break;
                        case "VA" : $channel="Virtual Account Maybank";break;
                        case "BT" : $channel="Virtual Account Permata";break;
                        case "B1" : $channel="Virtual Account Cimb Niaga";break;
                        case "A1" : $channel="Virtual Account ATM Bersama";break;
                        case "I1" : $channel="Virtual Account BNI";break;
                        case "M1" : $channel="Virtual Account Mandiri";break;
                        case "M2" : $channel="Virtual Account Mandiri H2H";break;
                        case "OV" : $channel="OVO";break;
                        case "OL" : $channel="OVO Link";break;
                        case "SP" : $channel="Shopeepay QRIS";break;
                        case "SA" : $channel="Shopeepay Apps";break;
                        case "SL" : $channel="Shopeepay Link";break;
                        case "DN" : $channel="Indodana Paylater";break;
                        case "LA" : $channel="LinkAja";break;
                        case "VC" : $channel="Credit Card";break;
                        case "FT" : $channel="Retail";break;
                        case "IR" : $channel="Indomaret";break;
                        default : $channel = "Virtual Account BCA";break;
                    }
                    ?>
                    <p><?= $channel ?></p>
                    <?php if($channel != "Cash"){ ?>
                    <div class="d-flex align-items-center justify-content-center gap-1">
                        <div class="h4 m-0"><?= (isset($trans["data"]["payment_create"]["vaNumber"])) ? $trans["data"]["payment_create"]["vaNumber"] : "-" ?></div>
                        <a href="#" title="copy to clipboard"><img src="<?= base_url("assets") ?>/images/copy-icon.svg" width="24" alt="" /></a>
                    </div>
                    <?php } ?>
                </div>
                <div style="display:block;margin:15px 0;height:1px;background: rgb(2,181,211);background: linear-gradient(90deg, rgba(2,181,211,1) 0%, rgba(230,240,205,1) 100%);">&nbsp;</div>
                <div>
                    <p>Billing Details</p>
                    <h3><?= $plan->name ?></h3>
                    <div class="row">
                        <div class="col-6">
                            <p>ID Invoice</p>
                            <div class="h6"><?= $trans["data"]["invoice_no"] ?></div>
                        </div>
                        <div class="col-6">
                            <p>Invoice Date</p>
                            <div class="h6"><?= date("d F Y H:i", strtotime($trans["data"]["created_at"])) ?> WIB</div>
                        </div>
                    </div>
                </div>
                <div style="display:block;margin:15px 0;height:1px;background: rgb(2,181,211);background: linear-gradient(90deg, rgba(2,181,211,1) 0%, rgba(230,240,205,1) 100%);">&nbsp;</div>
                <?php if($channel != "Cash"){ ?>
                <p>How To Pay</p>
                <div class="accordion accordion-flush hide" id="accordionFlushExample">
                    <div class="accordion-item bg-transparent text-white">
                      <h2 class="accordion-header" id="flush-headingOne">
                        <button class="accordion-button collapsed bg-transparent text-white px-0" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                          ATM Banking
                        </button>
                      </h2>
                      <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                        <div class="accordion-body px-0">
                            <ol>
                                <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                                <li>Aenean at semper tellus.</li>
                                <li>Ut cursus dapibus purus, id viverra arcu pulvinar eu.</li>
                                <li>Mauris eu dolor sit amet velit mollis rutrum auctor eu sem.</li>
                            </ol>
                        </div>
                      </div>
                    </div>
                    <div class="accordion-item bg-transparent text-white">
                      <h2 class="accordion-header" id="flush-headingTwo">
                        <button class="accordion-button collapsed bg-transparent text-white px-0" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
                          Mobile E-Banking
                        </button>
                      </h2>
                      <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
                        <div class="accordion-body px-0">
                            <ol>
                                <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                                <li>Aenean at semper tellus.</li>
                                <li>Ut cursus dapibus purus, id viverra arcu pulvinar eu.</li>
                                <li>Mauris eu dolor sit amet velit mollis rutrum auctor eu sem.</li>
                            </ol>
                        </div>
                      </div>
                    </div>
                    <div class="accordion-item bg-transparent text-white">
                      <h2 class="accordion-header" id="flush-headingThree">
                        <button class="accordion-button collapsed bg-transparent text-white px-0" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
                          Mobile Banking
                        </button>
                      </h2>
                      <div id="flush-collapseThree" class="accordion-collapse collapse" aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
                        <div class="accordion-body px-0">
                            <ol>
                                <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</li>
                                <li>Aenean at semper tellus.</li>
                                <li>Ut cursus dapibus purus, id viverra arcu pulvinar eu.</li>
                                <li>Mauris eu dolor sit amet velit mollis rutrum auctor eu sem.</li>
                            </ol>
                        </div>
                      </div>
                    </div>
                </div>
                <?php } ?>
            </div>
            <div class="card-footer">
                <a target="_blank" href="https://api.whatsapp.com/send?phone=<?= $cust->phone ?>&text=<?= (isset($trans["data"]["payment_create"]["paymentUrl"])) ? urlencode($trans["data"]["payment_create"]["paymentUrl"]) : "#" ?>" class="btn btn-primary w-100 text-uppercase mb-3">complete transaction</a>
                <a href="<?= base_url("menu") ?>" class="btn btn-outline-primary w-100 text-uppercase">Back to Menu</a>
            </div>
        </div>
    </main>

    <!-- Modal -->
    <div class="modal fade" id="modalSubTotal" tabindex="-1" aria-labelledby="modalSubTotalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title text-dark fs-5" id="modalSubTotalLabel">Checkout</h1>
                </div>
                <div class="modal-body">
                    <h5 class="text-dark mb-3"><?= ucwords($plan->tag)." ".$plan->name ?></h5>
                    <div class="d-flex align-items-center justify-content-between">
                        <h5 class="text-primary m-0 flex-shrink-0">Rp <?= number_format($plan->price, 0, ",", ".") ?></h5>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                    <a href="checkout.html" class="btn btn-primary">Checkout - Rp <?= number_format($plan->price, 0, ",", ".") ?></a>
                </div>
            </div>
        </div>
    </div>

    <!-- javascript -->
    <script src="<?= base_url("assets") ?>/js/jquery-3.6.1.min.js"></script>
    <script src="<?= base_url("assets") ?>/js/bootstrap.bundle.min.js"></script>
    <script src="<?= base_url("assets") ?>/js/script.js"></script>
</body>
</html>
