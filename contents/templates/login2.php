<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="x-apple-disable-message-reformatting">
    <title>Salesman Login page</title>
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@200;300;400;600;700;800;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?= base_url("assets") ?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?= base_url("assets") ?>/css/style.css">
</head>
<body>
    <header>

    </header>
    <main>
        <div class="container">
            <div style="text-align: center;padding: 30px 0;"><img src="<?= base_url("assets") ?>/images/LOGO.svg" alt="" /></div>
            <div class="card-header">
                <h3>Sign in</h3>
                <p>Salesman Division</p>
            </div>
            <div class="card-body">
                <form method="POST" action="<?= base_url("auth/sales/token") ?>">
                    <div class="form-group">
                        <input type="email" class="form-control" name="userdu" placeholder="Email Address" id="inputEmail">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name="passdu"  placeholder="Password" id="inputPassword">
                        <img src="<?= base_url("assets") ?>/images/eye-off.svg" alt="" width="36" onclick="showHidePassword()" id="iconPassword" class="position-absolute end-0 top-0 d-flex align-items-center justify-content-center p-2 h-100 cursor-pointer"/>
                    </div>
                    <a href="" class="text-primary mb-3 d-block text-end">Forgot Password?</a>
                    <button type="submit" class="btn-primary w-100 text-center mb-3">Sign in</button>
                    <!-- <a href="" class="btn w-100 text-center"><span class="text-white">Don't have an account?</span> <span class="text-primary"><b>Sign Up</b></span></a> -->
                </form>
            </div>
        </div>
    </main>
    <footer>
        <p>Follow us on</p>
        <div>
            <a href="#" style="margin:0 1rem;display: inline-block;"><img src="<?= base_url("assets") ?>/images/facebook-icon.svg" width="14" alt="" /></a>
            <a href="#" style="margin:0 1rem;display: inline-block;"><img src="<?= base_url("assets") ?>/images/instagram-icon.svg" width="14" alt="" /></a>
            <a href="#" style="margin:0 1rem;display: inline-block;"><img src="<?= base_url("assets") ?>/images/twitter-icon.svg" width="14" alt="" /></a>
            <a href="#" style="margin:0 1rem;display: inline-block;"><img src="<?= base_url("assets") ?>/images/youtube-icon.svg" width="14" alt="" /></a>
            <a href="#" style="margin:0 1rem;display: inline-block;"><img src="<?= base_url("assets") ?>/images/tiktok-icon.svg" width="14" alt="" /></a>
        </div>
        <div>&copy; copyright 2023 GRID. All right reserved.</div>
    </footer>

    <!-- javascript -->
    <script src="<?= base_url("assets") ?>/js/jquery-3.6.1.min.js"></script>
    <script src="<?= base_url("assets") ?>/js/bootstrap.bundle.min.js"></script>
    <script type="module" src="https://unpkg.com/ionicons@5.4.0/dist/ionicons/ionicons.esm.js"></script>
    <script nomodule="" src="https://unpkg.com/ionicons@5.4.0/dist/ionicons/ionicons.js"></script>
    <script defer src="<?= base_url() ?>assets/js/slick.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="<?= base_url("assets") ?>/js/script.js"></script>
    <script>
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    });
    <?php
    if(!empty($this->session->flashdata('feedback'))){
        $feedback = $this->session->flashdata('feedback');
        $message = (isset($feedback['message'])) ? $feedback["message"] : "error";
        if(isset($feedback['status']) && $feedback["status"] == "success"){
        ?>
          Toast.fire({
            icon: 'success',
            title: '<?= $message ?>'
          });
        <?php
        }else{
          ?>
          Toast.fire({
            icon: 'error',
            title: '<?= $message ?>'
          });
          <?php
        }
     } ?>
    </script>
</body>
</html>
