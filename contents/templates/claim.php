<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <title>Dewa United - Loket Tiket</title>

  <link rel="icon" type="image/ico" href="<?= base_url() ?>assets/img/favicon.ico"/>

  <link rel="stylesheet" href="<?= base_url() ?>assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/css/style.css">
</head>
<body>
  <header>
    <section class="header-section">
      <div class="container">
        <div class="row align-items-center">
          <div class="col-10 col-md-11">
            <h5 class="m-0">Riwayat Klaim</h5>
          </div>
          <div class="col-2 col-md-1">
            <a href="<?= base_url("auth/du/staff/logout") ?>" class="d-flex align-items-center justify-content-end">
              <ion-icon name="log-out-outline" style="width:24px;height:24px;"></ion-icon>
            </a>
          </div>
        </div>
      </div>
    </section>
  </header>
  <main>
    <section class="history-section">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <?php if(!empty($claim)){ ?>
            <ul>
              <?php foreach($claim as $cl){ ?>
              <li>
                <h5 class="fw-normal">Tiket #<?= $cl->id ?></h5>
                <p class="fw-bold"><?= $cl->ticket ?></p>
                <div class="d-flex align-items-center justify-content-between">
                  <small class="fw-normal text-secondary"><?= date("F d, Y - H:i", strtotime($cl->tgl)) ?></small>
                  <small class="text-<?= (!empty($cl->status)) ? "success" : "danger" ?>"><?= (!empty($cl->status)) ? 'Berhasil diklaim' : 'Kadaluarsa' ?></small>
                </div>
              </li>
              <?php } ?>
            </ul>
            <?php } ?>
          </div>
        </div>
      </div>
    </section>
  </main>
  <footer class="fixed-bottom">
    <section>
      <div class="container p-0 border-top">
        <div class="d-flex align-items-center justify-content-between">
          <a href="<?=  base_url("admin/claim/scan") ?>" class="nav-bottom">Scan</a>
          <div class="divider-vertical"></div>
          <a href="<?=  base_url("admin/claim/history") ?>" class="nav-bottom active">History</a>
        </div>
      </div>
    </section>
  </footer>

  <script src="<?= base_url() ?>assets/js/jquery-3.5.1.min.js"></script>
  <script src="<?= base_url() ?>assets/js/bootstrap.bundle.min.js"></script>
  <script type="module" src="https://unpkg.com/ionicons@5.4.0/dist/ionicons/ionicons.esm.js"></script>
  <script nomodule="" src="https://unpkg.com/ionicons@5.4.0/dist/ionicons/ionicons.js"></script>
  <script defer src="<?= base_url() ?>assets/js/slick.min.js"></script>
  <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
  <script src="https://unpkg.com/html5-qrcode@2.0.9/dist/html5-qrcode.min.js"></script>
  <script defer src="<?= base_url() ?>assets/js/script.js"></script>
</body>
</html>
