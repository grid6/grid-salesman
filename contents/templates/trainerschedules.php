<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="x-apple-disable-message-reformatting">
    <title>Select Course</title>
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@200;300;400;600;700;800;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?= base_url("assets") ?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?= base_url("assets") ?>/css/style.css">
</head>
<body>
    <header>
        <div class="header-nav">
            <a href="<?= base_url("courses") ?>" class="d-flex align-items-center z-index-1"><img src="<?= base_url("assets") ?>/images/arrow-icon.svg" alt=""/></a>
            <h4 class="position-absolute text-center start-0 end-0 m-0">Select Courses</h4>
        </div>
    </header>
    <main>
        <div class="container">
            <div class="card-body p-0">
                <div class="account-card border-0">
                    <?php
                    preg_match_all('/(?<=\b)\w/iu',$cust->name, $matches);
                    $avatar=mb_strtoupper(implode('',$matches[0]));
                    ?>
                    <div class="avatar"><?= $avatar ?></div>
                    <div class="d-flex flex-column gap-1 text-white">
                        <h5 class="m-0"><?= $cust->name ?></h5>
                        <p class="m-0"><?= $cust->email ?></p>
                    </div>
                </div>
                <hr>
                <div class="text-center">

                    <div class="row">
                      <div class="pricing-item">
                          <div class="pricing-price">
                              <p class="m-0"><strong>Schedule :</strong> <?= date("d M Y", strtotime($date)) ?></p>
                          </div>
                      </div>
                    </div>
                    <?php if($plan && $plan->num_rows() > 0){ ?>
                    <?php foreach($plan->result() as $pln){ ?>
                    <div class="row">
                          <div class="col-lg-12 col-12">
                              <div class="pricing-item">
                                  <div class="pricing-price">
                                    <h5 class="text-primary"><?= $pln->name ?></h5>
                                      <p class="m-1"><?= $pln->time_start." - ".$pln->time_finish ?></p>
                                  </div>

                                  <div class="pricing-button">
                                      <button type="button" class="btn btn-primary w-100 btnBuy" data-id="<?= $pln->trainer_id ?>" data-schedule="<?= $pln->id ?>" data-user="<?= $cust->id ?>" data-times="<?= $pln->time_start." - ".$pln->time_finish ?>" data-name="<?= $pln->name ?>">
                                        Book
                                      </button>
                                  </div>
                              </div>
                          </div>
                    </div>
                    <?php } ?>
                    <?php  } else { ?>
                        <div class="pricing-item">
                            <div class="pricing-price">
                                <p class="m-0">No Class Schedule on <?= $date ?></p>
                            </div>
                        </div>
                    <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <footer>
        <p>Follow us on</p>
        <div>
            <a href="#" style="margin:0 1rem;display: inline-block;"><img src="<?= base_url("assets") ?>/images/facebook-icon.svg" width="14" alt="" /></a>
            <a href="#" style="margin:0 1rem;display: inline-block;"><img src="<?= base_url("assets") ?>/images/instagram-icon.svg" width="14" alt="" /></a>
            <a href="#" style="margin:0 1rem;display: inline-block;"><img src="<?= base_url("assets") ?>/images/twitter-icon.svg" width="14" alt="" /></a>
            <a href="#" style="margin:0 1rem;display: inline-block;"><img src="<?= base_url("assets") ?>/images/youtube-icon.svg" width="14" alt="" /></a>
            <a href="#" style="margin:0 1rem;display: inline-block;"><img src="<?= base_url("assets") ?>/images/tiktok-icon.svg" width="14" alt="" /></a>
        </div>
        <div>&copy; copyright 2023 GRID. All right reserved.</div>
    </footer>

    <!-- Modal -->
    <div class="modal fade" id="modalSubTotal" tabindex="-1" aria-labelledby="modalSubTotalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title text-dark fs-5" id="modalSubTotalLabel">Checkout</h1>
                </div>
                <div class="modal-body">
                    <h5 class="text-dark mb-3" id="namePlan">CORPORATE PACKAGE - CORP MEMBER</h5>
                    <div class="d-flex align-items-center justify-content-between">

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                    <a id="btnCheckout" href="<?= base_url("courses/checkout/".$cust->id) ?>" class="btn btn-primary">Book Now</a>
                </div>
            </div>
        </div>
    </div>

    <!-- javascript -->
    <script src="<?= base_url("assets") ?>/js/jquery-3.6.1.min.js"></script>
    <script src="<?= base_url("assets") ?>/js/bootstrap.bundle.min.js"></script>
    <script src="<?= base_url("assets") ?>/js/script.js"></script>
    <script>
      $(document).on("click", ".btnBuy", function(e){
          var id = $(this).data("id");
          var nametag = $(this).data("name");
          var sched = $(this).data("schedule");
          var times = $(this).data("times");
          var date = btoa("<?= $date ?>");
          var btnhref = $("#btnCheckout").attr("href");
          $("#btnCheckout").attr("href", "<?= base_url("trainer/bookcheck/".$cust->id) ?>/"+id+"/"+sched+"?vzyt="+date);
          $("#namePlan").html(nametag+" - "+times);
          $("#modalSubTotal").modal("toggle");
      });
    </script>
</body>
</html>
