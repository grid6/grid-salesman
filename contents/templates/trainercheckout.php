<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="x-apple-disable-message-reformatting">
    <title>Checkout</title>
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@200;300;400;600;700;800;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?= base_url("assets") ?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?= base_url("assets") ?>/css/style.css">
</head>
<body>
    <header>
        <div class="header-nav">
            <a href="<?= base_url("member/plans/".$cust->id) ?>" class="d-flex align-items-center z-index-1"><img src="<?= base_url("assets") ?>/images/arrow-icon.svg" alt=""/></a>
            <h4 class="position-absolute text-center start-0 end-0 m-0">Checkout</h4>
        </div>
    </header>
    <main>
        <div class="container">
            <div class="card-body">
                <p>Select Payment Method</p>
                <div class="list-group">
                    <a href="<?= base_url("trainer/checkout/".$cust->id."/".$trainer->id."/$sessions/CASH/0") ?>" class="list-group-item list-group-item-action rounded-0">
                      <div class="d-flex w-100 justify-content-between">
                            <div class="d-flex align-items-center gap-3">
                                <h5 class="m-0">Cash</h5>
                            </div>
                            <div class="chevron-forward"></div>
                      </div>
                    </a>
                    <?php if(isset($channel["data"])){ ?>
                      <?php foreach($channel["data"] as $cdata){ ?>
                      <a href="<?= base_url("trainer/checkout/".$cust->id."/".$trainer->id."/$sessions/".$cdata["paymentMethod"]."/".$cdata["totalFee"]) ?>" class="list-group-item list-group-item-action rounded-0">
                        <div class="d-flex w-100 justify-content-between">
                              <div class="d-flex align-items-center gap-3">
                                  <img src="<?= $cdata["paymentImage"] ?>" alt="" width="156px"/>
                                  <h5 class="m-0"><?= $cdata["paymentName"] ?> <br /><small>Fee : <?= number_format($cdata["totalFee"], 0, ",", ".") ?></small></h5>
                              </div>
                              <div class="chevron-forward"></div>
                        </div>
                      </a>
                      <?php } ?>
                    <?php } ?>
                </div>
            </div>
        </div>
    </main>
    <footer>
        <p>Follow us on</p>
        <div>
            <a href="#" style="margin:0 1rem;display: inline-block;"><img src="<?= base_url("assets") ?>/images/facebook-icon.svg" width="14" alt="" /></a>
            <a href="#" style="margin:0 1rem;display: inline-block;"><img src="<?= base_url("assets") ?>/images/instagram-icon.svg" width="14" alt="" /></a>
            <a href="#" style="margin:0 1rem;display: inline-block;"><img src="<?= base_url("assets") ?>/images/twitter-icon.svg" width="14" alt="" /></a>
            <a href="#" style="margin:0 1rem;display: inline-block;"><img src="<?= base_url("assets") ?>/images/youtube-icon.svg" width="14" alt="" /></a>
            <a href="#" style="margin:0 1rem;display: inline-block;"><img src="<?= base_url("assets") ?>/images/tiktok-icon.svg" width="14" alt="" /></a>
        </div>
        <div>&copy; copyright 2023 GRID. All right reserved.</div>
    </footer>

    <!-- Modal -->
    <div class="modal fade" id="modalAddCust" tabindex="-1" aria-labelledby="modalAddCustLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header border-0">
                    <h1 class="modal-title text-dark fs-5" id="modalAddCustLabel">Add Customer</h1>
                </div>
                <div class="modal-body">
                    <div class="account-card border-0">
                        <div class="avatar">KL</div>
                        <div class="d-flex flex-column gap-1">
                            <h5 class="m-0">Karina Liverina</h5>
                            <p class="m-0">karina.liverina@gmail.com</p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer border-0">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                    <a href="select-package-membership.html" class="btn btn-primary">Continue</a>
                </div>
            </div>
        </div>
    </div>

    <!-- javascript -->
    <script src="<?= base_url("assets") ?>/js/jquery-3.6.1.min.js"></script>
    <script src="<?= base_url("assets") ?>/js/bootstrap.bundle.min.js"></script>
    <script src="<?= base_url("assets") ?>/js/script.js"></script>
</body>
</html>
