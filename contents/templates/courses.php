<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="x-apple-disable-message-reformatting">
    <title>Form Customer Registration</title>
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@200;300;400;600;700;800;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?= base_url("assets") ?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?= base_url("assets") ?>/css/style.css">
</head>
<body>
    <header>
        <div class="header-nav">
            <a href="<?= base_url("menu") ?>" class="d-flex align-items-center z-index-1"><img src="<?= base_url("assets") ?>/images/arrow-icon.svg" alt=""/></a>
            <h4 class="position-absolute text-center start-0 end-0 m-0">Add Customer</h4>
        </div>
    </header>
    <main>
        <div class="container">
            <div class="card-body">
                <form>
                    <p>Input your customer email address to book a class</p>
                    <div class="form-group">
                        <input type="email" class="form-control" placeholder="Email Address" id="inputEmail">
                    </div>
                    <a href="javascript:void(0)" id="btnUserz" class="btn-primary w-100 text-center mb-3 cursor-pointer">Check</a>
                </form>
            </div>
        </div>
    </main>
    <footer>
        <p>Follow us on</p>
        <div>
            <a href="#" style="margin:0 1rem;display: inline-block;"><img src="<?= base_url("assets") ?>/images/facebook-icon.svg" width="14" alt="" /></a>
            <a href="#" style="margin:0 1rem;display: inline-block;"><img src="<?= base_url("assets") ?>/images/instagram-icon.svg" width="14" alt="" /></a>
            <a href="#" style="margin:0 1rem;display: inline-block;"><img src="<?= base_url("assets") ?>/images/twitter-icon.svg" width="14" alt="" /></a>
            <a href="#" style="margin:0 1rem;display: inline-block;"><img src="<?= base_url("assets") ?>/images/youtube-icon.svg" width="14" alt="" /></a>
            <a href="#" style="margin:0 1rem;display: inline-block;"><img src="<?= base_url("assets") ?>/images/tiktok-icon.svg" width="14" alt="" /></a>
        </div>
        <div>&copy; copyright 2023 GRID. All right reserved.</div>
    </footer>

    <!-- Modal -->
    <div class="modal fade" id="modalAddCust" tabindex="-1" aria-labelledby="modalAddCustLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header border-0">
                    <h1 class="modal-title text-dark fs-5" id="modalAddCustLabel">Booking Class GRID Fitness Hub</h1>
                </div>
                <div class="modal-body">
                    <div class="account-card border-0">
                        <div id="usAvatar" class="avatar">KL</div>
                        <div class="d-flex flex-column gap-1">
                            <h5 id="usName" class="m-0">Karina Liverina</h5>
                            <p id="usEmail" class="m-0">karina.liverina@gmail.com</p>
                            <p id="usBal" class="m-0">0</p>
                            <div class="m-0">
                                <input type='date' id='book_date' class='form-control' />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer border-0">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                    <button id="btnContinue" class="btn btn-primary">Continue</button>
                </div>
            </div>
        </div>
    </div>

    <!-- javascript -->
    <script src="<?= base_url("assets") ?>/js/jquery-3.6.1.min.js"></script>
    <script src="<?= base_url("assets") ?>/js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript">
      $(document).on("click", "#btnUserz", function(e){
        e.stopPropagation();
        $.ajax({
            type: "POST",
            url: "<?= base_url() . 'custcheck' ?>",
            data: {
                'email': $("#inputEmail").val()
            },
            success: function(resp)
            {
                var rezp = JSON.parse(resp);
                $("#usAvatar").html(rezp.data.avatar);
                $("#usName").html(rezp.data.name);
                $("#usEmail").html(rezp.data.email);
                $("#usBal").html("Balance : "+rezp.data.balance.toLocaleString('en'));
                $("#btnContinue").attr("data-user", rezp.data.id);
                $("#modalAddCust").modal("toggle");
                console.log(rezp);
                // $("#getCodeModal").modal("toggle");
            }
          });
      });
      $(document).on("click", "#btnContinue", function(e){
        e.stopPropagation();
        var user = $(this).data("user");
        var bookdate = $("#book_date").val();
        if(bookdate != ""){
            bookdate = btoa(bookdate);
        }
        // alert(bookdate);
        window.location.href = "<?= base_url("courses/check") ?>/"+user+"?vzyt="+bookdate;
      });
    </script>
</body>
</html>
