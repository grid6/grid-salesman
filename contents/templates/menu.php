<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="x-apple-disable-message-reformatting">
    <title>Salesman Site</title>
    <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@200;300;400;600;700;800;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?= base_url("assets") ?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?= base_url("assets") ?>/css/style.css">
</head>
<body>
    <header>

    </header>
    <main>
        <div class="container">
            <div style="text-align: center;padding: 30px 0;"><img src="<?= base_url("assets") ?>/images/LOGO.svg" alt="" /></div>
            <div class="card-header">
                <h3>Hi, <?= $users->name ?></h3>
                <p>Salesman Site</p>
            </div>
            <div class="card-body">
                <div class="list-group">
                    <a href="<?= base_url("customers") ?>" class="list-group-item list-group-item-action rounded-0">
                      <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1">Customer Registration</h5>
                            <div class="chevron-forward"></div>
                      </div>
                    </a>
                    <a href="<?= base_url("enroll") ?>" class="list-group-item list-group-item-action rounded-0">
                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1">Enrolling Membership</h5>
                            <div class="chevron-forward"></div>
                        </div>
                    </a>
                    <a href="<?= base_url("credit") ?>" class="list-group-item list-group-item-action rounded-0">
                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1">Buy Credit</h5>
                            <div class="chevron-forward"></div>
                        </div>
                    </a>
                    <a href="<?= base_url("trainer") ?>" class="list-group-item list-group-item-action rounded-0">
                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1">Buy Trainer Session</h5>
                            <div class="chevron-forward"></div>
                        </div>
                    </a>
                    <a href="<?= base_url("courses") ?>" class="list-group-item list-group-item-action rounded-0">
                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1">Booking Class</h5>
                            <div class="chevron-forward"></div>
                        </div>
                    </a>
                    <a href="<?= base_url("trainer/book") ?>" class="list-group-item list-group-item-action rounded-0">
                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1">Booking PT Session</h5>
                            <div class="chevron-forward"></div>
                        </div>
                    </a>
                    <a href="<?= base_url("auth/sales/logout") ?>" class="list-group-item list-group-item-action rounded-0 border-0">
                        <div class="d-flex gap-1 w-100 text-danger">
                            <div class="log-out"></div>
                            <h5 class="mb-1">Sign Out</h5>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </main>
    <footer>
        <p>Follow us on</p>
        <div>
            <a href="#" style="margin:0 1rem;display: inline-block;"><img src="<?= base_url("assets") ?>/images/facebook-icon.svg" width="14" alt="" /></a>
            <a href="#" style="margin:0 1rem;display: inline-block;"><img src="<?= base_url("assets") ?>/images/instagram-icon.svg" width="14" alt="" /></a>
            <a href="#" style="margin:0 1rem;display: inline-block;"><img src="<?= base_url("assets") ?>/images/twitter-icon.svg" width="14" alt="" /></a>
            <a href="#" style="margin:0 1rem;display: inline-block;"><img src="<?= base_url("assets") ?>/images/youtube-icon.svg" width="14" alt="" /></a>
            <a href="#" style="margin:0 1rem;display: inline-block;"><img src="<?= base_url("assets") ?>/images/tiktok-icon.svg" width="14" alt="" /></a>
        </div>
        <div>&copy; copyright 2023 GRID. All right reserved.</div>
    </footer>

    <!-- javascript -->
    <script src="<?= base_url("assets") ?>/js/jquery-3.6.1.min.js"></script>
    <script src="<?= base_url("assets") ?>/js/bootstrap.bundle.min.js"></script>
    <script src="<?= base_url("assets") ?>/js/script.js"></script>
</body>
</html>
