<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <title>Dewa United - Loket Tiket</title>

  <link rel="icon" type="image/ico" href="<?= base_url() ?>assets/img/favicon.ico"/>

  <link rel="stylesheet" href="<?= base_url() ?>assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/css/style.css">
</head>
<body>
  <header>
    <section class="header-section">
      <div class="container bg-transparent text-white">
        <div class="row align-items-center">
          <div class="col-10 col-md-11">
            <h5 class="m-0">Scan Ticket DewaUnited</h5>
          </div>
          <div class="col-2 col-md-1">
            <a href="<?= base_url("auth/du/staff/logout") ?>" class="d-flex align-items-center justify-content-end">
              <ion-icon name="log-out-outline" style="width:24px;height:24px;"></ion-icon>
            </a>
          </div>
        </div>
      </div>
    </section>
  </header>
  <main>
    <section class="scan-section">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div id="qr-reader"></div>
          </div>
          <div class="col-12">
            <div class="position-relative">
              <button type="submit" class="btn btn-primary position-absolute end-0 h-100" onclick="submitQR()">Submit</button>
              <input type="text" class="form-control form-control-lg" placeholder="Masukkan kode QR">
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>
  <footer class="fixed-bottom">
    <section>
      <div class="container p-0 border-top">
        <div class="d-flex align-items-center justify-content-between">
          <a href="<?= base_url("admin/claim/scan") ?>" class="nav-bottom active">Scan</a>
          <div class="divider-vertical"></div>
          <a href="<?= base_url("admin/claim/history") ?>" class="nav-bottom">History</a>
        </div>
      </div>
    </section>
  </footer>

  <script src="<?= base_url() ?>assets/js/jquery-3.5.1.min.js"></script>
  <script src="<?= base_url() ?>assets/js/bootstrap.bundle.min.js"></script>
  <script type="module" src="https://unpkg.com/ionicons@5.4.0/dist/ionicons/ionicons.esm.js"></script>
  <script nomodule="" src="https://unpkg.com/ionicons@5.4.0/dist/ionicons/ionicons.js"></script>
  <script defer src="<?= base_url() ?>assets/js/slick.min.js"></script>
  <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
  <script src="https://unpkg.com/html5-qrcode@2.0.9/dist/html5-qrcode.min.js"></script>
  <script defer src="<?= base_url() ?>assets/js/script.js"></script>
  <script type="text/javascript">

  function onScanSuccess(decodedText, decodedResult) {
      console.log(`Code scanned = ${decodedText}`, decodedResult);
      $.ajax({
          url : "<?= base_url() ?>api/claim/ticket",
          type : "POST",
          data : { code : decodedText },
          success: function(resps){
              var rezp = JSON.parse(resps);
              if(rezp.hasOwnProperty('data')){
                Swal.fire({
                  title: rezp.message,
                  text: rezp.data.produk,
                  // icon: 'warning',
                  showCancelButton: false,
                  confirmButtonColor: '#dbb76f',
                  cancelButtonColor: '#ccc',
                  confirmButtonText: 'OK',
                  cancelButtonText: 'Batal'
                }).then((result) => {
                  
                });
              }else{
                Swal.fire({
                  title: 'Gagal Diklaim',
                  text: rezp.message,
                  // icon: 'warning',
                  showCancelButton: false,
                  confirmButtonColor: '#dbb76f',
                  cancelButtonColor: '#ccc',
                  confirmButtonText: 'OK',
                  cancelButtonText: 'Batal'
                }).then((result) => {

                });
              }
              console.log(rezp);
          }
      });
  }

  var html5QrcodeScanner = new Html5QrcodeScanner(
    "qr-reader", { fps: 10, qrbox: 250 });

  html5QrcodeScanner.render(onScanSuccess);
  </script>
</body>
</html>
